﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect.Function;
using NHibernate.Tool.hbm2ddl;
using ORMBenchmarking.NHibernate.Helpers;
using System.Reflection;

namespace ORMBenchmarking.NHibernate
{
    public static class FluentNHibernateSession
    {
        public static object HibernateUtil { get; private set; }

        public static ISession OpenSession()
        {
            var configuration = new Configuration();
            configuration.AddSqlFunction("stdev", new StandardSQLFunction("stdev", NHibernateUtil.Double));
            string connectionString = "Server=DESKTOP-H4PHU4K;Database=AdventureWorks2014;Trusted_Connection=True;Connection Timeout=300";
            ISessionFactory sessionFactory = Fluently.Configure(configuration)
                .Database(MsSqlConfiguration.MsSql2012
                  .ConnectionString(connectionString).ShowSql()
                )
                .Mappings(m =>
                          m.FluentMappings
                            .AddFromAssembly(Assembly.GetExecutingAssembly()))
                .ExposeConfiguration(cfg => new SchemaExport(cfg)
                .Create(false, false))
                .BuildSessionFactory();
            return sessionFactory.OpenSession();
        }
    }
}
