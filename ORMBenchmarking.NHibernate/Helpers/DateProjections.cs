﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Dialect.Function;
using System;
using System.Linq.Expressions;

namespace ORMBenchmarking.NHibernate.Helpers
{
    public static class DateProjections
    {
        private const string DateDiffFormat = "datediff({0}, ?1, ?2)";

        public static IProjection GetAge(
            string datepart,
            Expression<Func<object>> birthDate)
        {
            string functionTemplate = string.Format(DateDiffFormat, datepart);

            return Projections.SqlFunction(
                new SQLFunctionTemplate(NHibernateUtil.Int32, functionTemplate),
                NHibernateUtil.Int32,
                Projections.Property(birthDate),
                Projections.Constant(DateTime.Now));
        }
    }
}
