﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 26.11.2018 21:43:16
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using ORMBenchmarking.NHibernate.Entities;

namespace ORMBenchmarking.NHibernate.Mappings
{
    /// <summary>
    /// There are no comments for ProductVendorMap in the schema.
    /// </summary>
    public partial class ProductVendorMap : ClassMap<ProductVendor>
    {
        /// <summary>
        /// There are no comments for ProductVendorMap constructor in the schema.
        /// </summary>
        public ProductVendorMap()
        {
              Schema(@"Purchasing");
              Table(@"ProductVendor");
              LazyLoad();
              CompositeId()
                .KeyProperty(x => x.ProductID, set => {
                    set.Type("Int32");
                    set.ColumnName("ProductID");
                    set.Access.Property(); } )
                .KeyProperty(x => x.BusinessEntityID, set => {
                    set.Type("Int32");
                    set.ColumnName("BusinessEntityID");
                    set.Access.Property(); } );
              Map(x => x.AverageLeadTime)    
                .Column("AverageLeadTime")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never().CustomSqlType("int")
                .Not.Nullable()
                .Precision(10);
              Map(x => x.StandardPrice)    
                .Column("StandardPrice")
                .CustomType("Decimal")
                .Access.Property()
                .Generated.Never().CustomSqlType("money")
                .Not.Nullable()
                .Precision(19)
                .Scale(4);
              Map(x => x.LastReceiptCost)    
                .Column("LastReceiptCost")
                .CustomType("Decimal")
                .Access.Property()
                .Generated.Never().CustomSqlType("money")
                .Precision(19)
                .Scale(4);
              Map(x => x.LastReceiptDate)    
                .Column("LastReceiptDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never().CustomSqlType("datetime");
              Map(x => x.MinOrderQty)    
                .Column("MinOrderQty")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never().CustomSqlType("int")
                .Not.Nullable()
                .Precision(10);
              Map(x => x.MaxOrderQty)    
                .Column("MaxOrderQty")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never().CustomSqlType("int")
                .Not.Nullable()
                .Precision(10);
              Map(x => x.OnOrderQty)    
                .Column("OnOrderQty")
                .CustomType("Int32")
                .Access.Property()
                .Generated.Never().CustomSqlType("int")
                .Precision(10);
              Map(x => x.ModifiedDate)    
                .Column("ModifiedDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never()
                .Default(@"getdate()").CustomSqlType("datetime")
                .Not.Nullable();
              References(x => x.Vendor)
                .Class<Vendor>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("BusinessEntityID");
              References(x => x.Product)
                .Class<Product>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("ProductID");
              References(x => x.UnitMeasure)
                .Class<UnitMeasure>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("UnitMeasureCode");
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
