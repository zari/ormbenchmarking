﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 26.11.2018 21:43:16
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using ORMBenchmarking.NHibernate.Entities;

namespace ORMBenchmarking.NHibernate.Mappings
{
    /// <summary>
    /// There are no comments for ProductProductPhotoMap in the schema.
    /// </summary>
    public partial class ProductProductPhotoMap : ClassMap<ProductProductPhoto>
    {
        /// <summary>
        /// There are no comments for ProductProductPhotoMap constructor in the schema.
        /// </summary>
        public ProductProductPhotoMap()
        {
              Schema(@"Production");
              Table(@"ProductProductPhoto");
              LazyLoad();
              CompositeId()
                .KeyProperty(x => x.ProductID, set => {
                    set.Type("Int32");
                    set.ColumnName("ProductID");
                    set.Access.Property(); } )
                .KeyProperty(x => x.ProductPhotoID, set => {
                    set.Type("Int32");
                    set.ColumnName("ProductPhotoID");
                    set.Access.Property(); } );
              Map(x => x.Primary)    
                .Column("`Primary`")
                .CustomType("Boolean")
                .Access.Property()
                .Generated.Never()
                .Default(@"0").CustomSqlType("bit")
                .Not.Nullable();
              Map(x => x.ModifiedDate)    
                .Column("ModifiedDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never()
                .Default(@"getdate()").CustomSqlType("datetime")
                .Not.Nullable();
              References(x => x.Product)
                .Class<Product>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("ProductID");
              References(x => x.ProductPhoto)
                .Class<ProductPhoto>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("ProductPhotoID");
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
