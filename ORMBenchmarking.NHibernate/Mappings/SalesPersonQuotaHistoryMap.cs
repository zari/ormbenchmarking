﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 26.11.2018 21:43:16
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using ORMBenchmarking.NHibernate.Entities;

namespace ORMBenchmarking.NHibernate.Mappings
{
    /// <summary>
    /// There are no comments for SalesPersonQuotaHistoryMap in the schema.
    /// </summary>
    public partial class SalesPersonQuotaHistoryMap : ClassMap<SalesPersonQuotaHistory>
    {
        /// <summary>
        /// There are no comments for SalesPersonQuotaHistoryMap constructor in the schema.
        /// </summary>
        public SalesPersonQuotaHistoryMap()
        {
              Schema(@"Sales");
              Table(@"SalesPersonQuotaHistory");
              LazyLoad();
              CompositeId()
                .KeyProperty(x => x.BusinessEntityID, set => {
                    set.Type("Int32");
                    set.ColumnName("BusinessEntityID");
                    set.Access.Property(); } )
                .KeyProperty(x => x.QuotaDate, set => {
                    set.Type("DateTime");
                    set.ColumnName("QuotaDate");
                    set.Access.Property(); } );
              Map(x => x.SalesQuota)    
                .Column("SalesQuota")
                .CustomType("Decimal")
                .Access.Property()
                .Generated.Never().CustomSqlType("money")
                .Not.Nullable()
                .Precision(19)
                .Scale(4);
              Map(x => x.Rowguid)    
                .Column("rowguid")
                .CustomType("Guid")
                .Access.Property()
                .Generated.Never()
                .Default(@"newid()").CustomSqlType("uniqueidentifier")
                .Not.Nullable();
              Map(x => x.ModifiedDate)    
                .Column("ModifiedDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never()
                .Default(@"getdate()").CustomSqlType("datetime")
                .Not.Nullable();
              References(x => x.SalesPerson)
                .Class<SalesPerson>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("BusinessEntityID");
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
