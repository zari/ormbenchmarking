﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 26.11.2018 21:43:16
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using ORMBenchmarking.NHibernate.Entities;

namespace ORMBenchmarking.NHibernate.Mappings
{
    /// <summary>
    /// There are no comments for SalesReasonMap in the schema.
    /// </summary>
    public partial class SalesReasonMap : ClassMap<SalesReason>
    {
        /// <summary>
        /// There are no comments for SalesReasonMap constructor in the schema.
        /// </summary>
        public SalesReasonMap()
        {
              Schema(@"Sales");
              Table(@"SalesReason");
              LazyLoad();
              Id(x => x.SalesReasonID)
                .Column("SalesReasonID")
                .CustomType("Int32")
                .Access.Property().CustomSqlType("int")
                .Not.Nullable()
                .Precision(10)                
                .GeneratedBy.Identity();
              Map(x => x.Name)    
                .Column("Name")
                .CustomType("String")
                .Access.Property()
                .Generated.Never().CustomSqlType("nvarchar(50)")
                .Not.Nullable()
                .Length(50);
              Map(x => x.ReasonType)    
                .Column("ReasonType")
                .CustomType("String")
                .Access.Property()
                .Generated.Never().CustomSqlType("nvarchar(50)")
                .Not.Nullable()
                .Length(50);
              Map(x => x.ModifiedDate)    
                .Column("ModifiedDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never()
                .Default(@"getdate()").CustomSqlType("datetime")
                .Not.Nullable();
              HasMany<SalesOrderHeaderSalesReason>(x => x.SalesOrderHeaderSalesReasons)
                .Access.Property()
                .AsSet()
                .Cascade.None()
                .LazyLoad()
                // .OptimisticLock.Version() /*bug (or missing feature) in Fluent NHibernate*/
                .Inverse()
                .Generic()
                .KeyColumns.Add("SalesReasonID", mapping => mapping.Name("SalesReasonID")
                                                                     .SqlType("int")
                                                                     .Not.Nullable());
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
