﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 26.11.2018 21:43:16
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using ORMBenchmarking.NHibernate.Entities;

namespace ORMBenchmarking.NHibernate.Mappings
{
    /// <summary>
    /// There are no comments for BusinessEntityAddressMap in the schema.
    /// </summary>
    public partial class BusinessEntityAddressMap : ClassMap<BusinessEntityAddress>
    {
        /// <summary>
        /// There are no comments for BusinessEntityAddressMap constructor in the schema.
        /// </summary>
        public BusinessEntityAddressMap()
        {
              Schema(@"Person");
              Table(@"BusinessEntityAddress");
              LazyLoad();
              CompositeId()
                .KeyProperty(x => x.BusinessEntityID, set => {
                    set.Type("Int32");
                    set.ColumnName("BusinessEntityID");
                    set.Access.Property(); } )
                .KeyProperty(x => x.AddressID, set => {
                    set.Type("Int32");
                    set.ColumnName("AddressID");
                    set.Access.Property(); } )
                .KeyProperty(x => x.AddressTypeID, set => {
                    set.Type("Int32");
                    set.ColumnName("AddressTypeID");
                    set.Access.Property(); } );
              Map(x => x.Rowguid)    
                .Column("rowguid")
                .CustomType("Guid")
                .Access.Property()
                .Generated.Never()
                .Default(@"newid()").CustomSqlType("uniqueidentifier")
                .Not.Nullable();
              Map(x => x.ModifiedDate)    
                .Column("ModifiedDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never()
                .Default(@"getdate()").CustomSqlType("datetime")
                .Not.Nullable();
              References(x => x.Address)
                .Class<Address>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("AddressID");
              References(x => x.AddressType)
                .Class<AddressType>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("AddressTypeID");
              References(x => x.BusinessEntity)
                .Class<BusinessEntity>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("BusinessEntityID");
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
