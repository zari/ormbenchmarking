﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate Fluent Mapping template.
// Code is generated on: 26.11.2018 21:43:16
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using ORMBenchmarking.NHibernate.Entities;

namespace ORMBenchmarking.NHibernate.Mappings
{
    /// <summary>
    /// There are no comments for CountryRegionCurrencyMap in the schema.
    /// </summary>
    public partial class CountryRegionCurrencyMap : ClassMap<CountryRegionCurrency>
    {
        /// <summary>
        /// There are no comments for CountryRegionCurrencyMap constructor in the schema.
        /// </summary>
        public CountryRegionCurrencyMap()
        {
              Schema(@"Sales");
              Table(@"CountryRegionCurrency");
              LazyLoad();
              CompositeId()
                .KeyProperty(x => x.CountryRegionCode, set => {
                    set.Type("String");
                    set.ColumnName("CountryRegionCode");
                    set.Length(3);
                    set.Access.Property(); } )
                .KeyProperty(x => x.CurrencyCode, set => {
                    set.Type("String");
                    set.ColumnName("CurrencyCode");
                    set.Length(3);
                    set.Access.Property(); } );
              Map(x => x.ModifiedDate)    
                .Column("ModifiedDate")
                .CustomType("DateTime")
                .Access.Property()
                .Generated.Never()
                .Default(@"getdate()").CustomSqlType("datetime")
                .Not.Nullable();
              References(x => x.Currency)
                .Class<Currency>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("CurrencyCode");
              References(x => x.CountryRegion)
                .Class<CountryRegion>()
                .Access.Property()
                .Cascade.None()
                .LazyLoad()
                .Columns("CountryRegionCode");
              ExtendMapping();
        }

        #region Partial Methods

        partial void ExtendMapping();

        #endregion
    }

}
