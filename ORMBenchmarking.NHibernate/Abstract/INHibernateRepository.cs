﻿using ORMBenchmarking.Models.ViewModel;
using ORMBenchmarking.NHibernate.Entities;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Abstract
{
    public interface INHibernateRepository
    {
        List<TransactionHistory> GetAllTransactions();
        List<TransactionHistoryArchive> GetAllArchiveTransactions();
        decimal GetSumOfAllTransactions();
        List<EmployeeAgeViewModel> GetEmployeesOlderThan50();
        List<SalesYearViewModel> GetSalesByYears();
        List<EmployeeQuantityPerDepartmentViewModel> GetEmployeeQuantityPerDepartment();
        List<SalesOrderWithProductInfoViewModel> GetSalesOrderDetailsWithProductInfo();
        List<GroupedOrderItemWithStatisticsViewModel> GetGroupedOrdersWithStatistics();
        void GetTransactionProductsWithInformation();
    }
}
