﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Dialect.Function;
using NHibernate.Transform;
using NHibernate.Type;
using ORMBenchmarking.Models.ViewModel;
using ORMBenchmarking.NHibernate.Abstract;
using ORMBenchmarking.NHibernate.Entities;
using ORMBenchmarking.NHibernate.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ORMBenchmarking.NHibernate.Repository
{
    public class NHibernateRepository : INHibernateRepository
    {
        private ISession _session;

        public NHibernateRepository()
        {
            _session = FluentNHibernateSession.OpenSession();
        }

        public List<TransactionHistory> GetAllTransactions()
        {
            return _session.Query<TransactionHistory>().ToList();
        }

        public List<TransactionHistoryArchive> GetAllArchiveTransactions()
        {
            return _session.Query<TransactionHistoryArchive>().ToList();
        }

        public decimal GetSumOfAllTransactions()
        {
            return _session.Query<TransactionHistory>().Sum(x => x.Quantity * x.ActualCost);
        }

        public List<EmployeeAgeViewModel> GetEmployeesOlderThan50()
        {
            Person personAlias = null;
            Employee employeeAlias = null;
            EmployeeAgeViewModel result = null;
            var dateNow = DateTime.Now;

            return _session.QueryOver(() => employeeAlias).JoinQueryOver(x => x.Person, () => personAlias).SelectList(
                list => list
                .Select(x => personAlias.FirstName).WithAlias(() => result.FirstName)
                .Select(x => personAlias.LastName).WithAlias(() => result.LastName)
                .Select(x => x.Gender).WithAlias(() => result.Gender)
                .Select(x => x.BirthDate).WithAlias(() => result.BirthDate)
                .Select(x => x.HireDate).WithAlias(() => result.HireDate)
                .Select(DateProjections.GetAge("yy", () => employeeAlias.BirthDate)).WithAlias(() => result.Age)
                )
                .Where(Restrictions.Ge(DateProjections.GetAge("dd", () => employeeAlias.BirthDate), 18262))
                .TransformUsing(Transformers.AliasToBean<EmployeeAgeViewModel>())
                .List<EmployeeAgeViewModel>()
                .OrderByDescending(x => x.Age)
                .ToList();
        }

        public List<SalesYearViewModel> GetSalesByYears()
        {
            SalesYearViewModel result = null;

            return _session.QueryOver<SalesOrderHeader>().SelectList(list => list
                        .Select(Projections.SqlGroupProjection("YEAR(OrderDate) AS [YEAR]", "YEAR(OrderDate)", new[] { "YEAR" },
                                new IType[] { NHibernateUtil.Int32 })).WithAlias(() => result.YearOfSummary)
                        .SelectCount(x => x.SalesOrderID).WithAlias(() => result.OrdersQuantity)
                        .SelectSum(x => x.SubTotal).WithAlias(() => result.OrdersPriceSum))
                .OrderByAlias(() => result.YearOfSummary).Asc
             .TransformUsing(Transformers.AliasToBean<SalesYearViewModel>())
             .List<SalesYearViewModel>()
             .ToList();
        }

        public List<EmployeeQuantityPerDepartmentViewModel> GetEmployeeQuantityPerDepartment()
        {
            Department departmentAlias = null;
            EmployeeDepartmentHistory employeeDepartmentHistoryAlias = null;
            EmployeeQuantityPerDepartmentViewModel result = null;

            return _session.QueryOver(() => departmentAlias)
                .JoinQueryOver(x => x.EmployeeDepartmentHistories, () => employeeDepartmentHistoryAlias)
                .Where(Restrictions.IsNull("EndDate"))
                .SelectList(list => list
                    .Select(Projections.SqlGroupProjection("Name AS [DepartmentName]", "Name", new[] { "DepartmentName" }, new IType[] { NHibernateUtil.String })).WithAlias(() => result.DepartmentName)
                    .Select(Projections.Count("DepartmentID").WithAlias(() => result.EmployeeQuantity)))
                .OrderByAlias(() => result.EmployeeQuantity).Desc
                .TransformUsing(Transformers.AliasToBean<EmployeeQuantityPerDepartmentViewModel>())
                .List<EmployeeQuantityPerDepartmentViewModel>()
                .ToList();
        }

        public List<SalesOrderWithProductInfoViewModel> GetSalesOrderDetailsWithProductInfo()
        {
            //SpecialOfferProduct specialOfferProductAlias = null;
            //SalesOrderDetail salesOrderDetailAlias = null;
            //Product productAlias = null;
            //UnitMeasure sizeUnitAlias = null;
            //UnitMeasure weightUnitAlias = null;
            //ProductSubcategory productSubcategoryAlias = null;
            //ProductModel productModelAlias = null;
            //SalesOrderWithProductInfoViewModel result = null;


            //return _session.QueryOver(() => specialOfferProductAlias)
            //    .JoinAlias(() => specialOfferProductAlias.Product, () => productAlias)
            //    .JoinAlias(() => specialOfferProductAlias.SalesOrderDetails, () => salesOrderDetailAlias)
            //    .JoinAlias(() => specialOfferProductAlias.Product.UnitMeasure_SizeUnitMeasureCode, () => sizeUnitAlias)
            //    .JoinAlias(() => specialOfferProductAlias.Product.UnitMeasure_WeightUnitMeasureCode, () => weightUnitAlias)
            //    .JoinAlias(() => specialOfferProductAlias.Product.ProductSubcategory, () => productSubcategoryAlias)
            //    .JoinAlias(() => specialOfferProductAlias.Product.ProductModel, () => productModelAlias).SelectList(
            //    list => list
            //         .Select(x => salesOrderDetailAlias.SalesOrderID).WithAlias(() => result.SalesOrderId)
            //         .Select(x => salesOrderDetailAlias.SalesOrderDetailID).WithAlias(() => result.SalesOrderDetailId)
            //         .Select(x => salesOrderDetailAlias.CarrierTrackingNumber).WithAlias(() => result.CarrierTrackingNumber)
            //         .Select(x => salesOrderDetailAlias.OrderQty).WithAlias(() => result.OrderQty)
            //         .Select(x => productAlias.ProductID).WithAlias(() => result.ProductId)
            //         .Select(x => specialOfferProductAlias.SpecialOfferID).WithAlias(() => result.SpecialOfferId)
            //         .Select(x => salesOrderDetailAlias.UnitPrice).WithAlias(() => result.UnitPrice)
            //         .Select(x => salesOrderDetailAlias.UnitPriceDiscount).WithAlias(() => result.UnitPriceDiscount)
            //         .Select(x => salesOrderDetailAlias.LineTotal).WithAlias(() => result.LineTotal)
            //         .Select(x => salesOrderDetailAlias.Rowguid).WithAlias(() => result.RowguidSalesOrder)
            //         .Select(x => salesOrderDetailAlias.ModifiedDate).WithAlias(() => result.ModifiedDateSalesOrder)
            //         .Select(x => productAlias.Name).WithAlias(() => result.Name)
            //         .Select(x => productAlias.ProductNumber).WithAlias(() => result.ProductNumber)
            //         .Select(x => productAlias.MakeFlag).WithAlias(() => result.MakeFlag)
            //         .Select(x => productAlias.FinishedGoodsFlag).WithAlias(() => result.FinishedGoodsFlag)
            //         .Select(x => productAlias.Color).WithAlias(() => result.Color)
            //         .Select(x => productAlias.SafetyStockLevel).WithAlias(() => result.SafetyStockLevel)
            //         .Select(x => productAlias.ReorderPoint).WithAlias(() => result.ReorderPoint)
            //         .Select(x => productAlias.StandardCost).WithAlias(() => result.StandardCost)
            //         .Select(x => productAlias.ListPrice).WithAlias(() => result.ListPrice)
            //         .Select(x => productAlias.Size).WithAlias(() => result.Size)
            //         .Select(x => sizeUnitAlias.UnitMeasureCode).WithAlias(() => result.SizeUnitMeasureCode)
            //         .Select(x => weightUnitAlias.UnitMeasureCode).WithAlias(() => result.WeightUnitMeasureCode)
            //         .Select(x => productAlias.Weight).WithAlias(() => result.Weight)
            //         .Select(x => productAlias.DaysToManufacture).WithAlias(() => result.DaysToManufacture)
            //         .Select(x => productAlias.ProductLine).WithAlias(() => result.ProductLine)
            //         .Select(x => productAlias.Class).WithAlias(() => result.Class)
            //         .Select(x => productAlias.Style).WithAlias(() => result.Style)
            //         .Select(x => productSubcategoryAlias.ProductSubcategoryID).WithAlias(() => result.ProductSubcategoryId)
            //         .Select(x => productModelAlias.ProductModelID).WithAlias(() => result.ProductModelId)
            //         .Select(x => productAlias.SellStartDate).WithAlias(() => result.SellStartDate)
            //         .Select(x => productAlias.SellEndDate).WithAlias(() => result.SellEndDate)
            //         .Select(x => productAlias.DiscontinuedDate).WithAlias(() => result.DiscontinuedDate)
            //         .Select(x => productAlias.Rowguid).WithAlias(() => result.RowguidProduct)
            //         .Select(x => productAlias.ModifiedDate).WithAlias(() => result.ModifiedDateProduct))
            //        .OrderByAlias(() => result.ProductId).Desc.OrderByAlias(() => result.SalesOrderId).Asc
            //        .TransformUsing(Transformers.AliasToBean<SalesOrderWithProductInfoViewModel>())
            //        .List<SalesOrderWithProductInfoViewModel>()
            //        .ToList();

            SpecialOfferProduct specialOfferProductAlias = null;
            SalesOrderDetail salesOrderDetailAlias = null;
            Product productAlias = null;
            UnitMeasure sizeUnitAlias = null;
            UnitMeasure weightUnitAlias = null;
            ProductSubcategory productSubcategoryAlias = null;
            ProductModel productModelAlias = null;
            SalesOrderWithProductInfoViewModel result = null;


            return _session.QueryOver(() => salesOrderDetailAlias)
                .JoinQueryOver(x => x.Product, () => productAlias)
                //.JoinQueryOver(x => x.SpecialOfferProducts, () => specialOfferProductAlias)
                //.JoinAlias(() => productAlias.UnitMeasure_SizeUnitMeasureCode, () => sizeUnitAlias)
                //.JoinAlias(() => productAlias.UnitMeasure_WeightUnitMeasureCode, () => weightUnitAlias)
                //.JoinAlias(() => specialOfferProductAlias.Product.ProductSubcategory, () => productSubcategoryAlias)
                //.JoinAlias(() => specialOfferProductAlias.Product.ProductModel, () => productModelAlias)
                .SelectList(
                list => list
                     .Select(x => salesOrderDetailAlias.SalesOrderID).WithAlias(() => result.SalesOrderId)
                     .Select(x => salesOrderDetailAlias.SalesOrderDetailID).WithAlias(() => result.SalesOrderDetailId)
                     .Select(x => salesOrderDetailAlias.CarrierTrackingNumber).WithAlias(() => result.CarrierTrackingNumber)
                     .Select(x => salesOrderDetailAlias.OrderQty).WithAlias(() => result.OrderQty)
                     .Select(x => productAlias.ProductID).WithAlias(() => result.ProductId)
                     //.Select(x => specialOfferProductAlias.SpecialOfferID).WithAlias(() => result.SpecialOfferId)
                     .Select(x => salesOrderDetailAlias.UnitPrice).WithAlias(() => result.UnitPrice)
                     .Select(x => salesOrderDetailAlias.UnitPriceDiscount).WithAlias(() => result.UnitPriceDiscount)
                     .Select(x => salesOrderDetailAlias.LineTotal).WithAlias(() => result.LineTotal)
                     .Select(x => salesOrderDetailAlias.Rowguid).WithAlias(() => result.RowguidSalesOrder)
                     .Select(x => salesOrderDetailAlias.ModifiedDate).WithAlias(() => result.ModifiedDateSalesOrder)
                     .Select(x => productAlias.Name).WithAlias(() => result.Name)
                     .Select(x => productAlias.ProductNumber).WithAlias(() => result.ProductNumber)
                     .Select(x => productAlias.MakeFlag).WithAlias(() => result.MakeFlag)
                     .Select(x => productAlias.FinishedGoodsFlag).WithAlias(() => result.FinishedGoodsFlag)
                     .Select(x => productAlias.Color).WithAlias(() => result.Color)
                     .Select(x => productAlias.SafetyStockLevel).WithAlias(() => result.SafetyStockLevel)
                     .Select(x => productAlias.ReorderPoint).WithAlias(() => result.ReorderPoint)
                     .Select(x => productAlias.StandardCost).WithAlias(() => result.StandardCost)
                     .Select(x => productAlias.ListPrice).WithAlias(() => result.ListPrice)
                     .Select(x => productAlias.Size).WithAlias(() => result.Size)
                     //.Select(x => sizeUnitAlias.UnitMeasureCode).WithAlias(() => result.SizeUnitMeasureCode)
                     //.Select(x => weightUnitAlias.UnitMeasureCode).WithAlias(() => result.WeightUnitMeasureCode)
                     .Select(x => productAlias.Weight).WithAlias(() => result.Weight)
                     .Select(x => productAlias.DaysToManufacture).WithAlias(() => result.DaysToManufacture)
                     .Select(x => productAlias.ProductLine).WithAlias(() => result.ProductLine)
                     .Select(x => productAlias.Class).WithAlias(() => result.Class)
                     .Select(x => productAlias.Style).WithAlias(() => result.Style)
                    //    .Select(x => productSubcategoryAlias.ProductSubcategoryID).WithAlias(() => result.ProductSubcategoryId)
                    //    .Select(x => productModelAlias.ProductModelID).WithAlias(() => result.ProductModelId)
                    .Select(x => productAlias.SellStartDate).WithAlias(() => result.SellStartDate)
                    .Select(x => productAlias.SellEndDate).WithAlias(() => result.SellEndDate)
                    .Select(x => productAlias.DiscontinuedDate).WithAlias(() => result.DiscontinuedDate)
                    .Select(x => productAlias.Rowguid).WithAlias(() => result.RowguidProduct)
                    .Select(x => productAlias.ModifiedDate).WithAlias(() => result.ModifiedDateProduct))
                    .OrderByAlias(() => result.ProductId).Desc.OrderByAlias(() => result.SalesOrderId).Asc
                    .TransformUsing(Transformers.AliasToBean<SalesOrderWithProductInfoViewModel>())
                    .List<SalesOrderWithProductInfoViewModel>()
                    .ToList();
        }

        public List<GroupedOrderItemWithStatisticsViewModel> GetGroupedOrdersWithStatistics()
        {
            GroupedOrderItemWithStatisticsViewModel result = null;
            var subtractFunction = new VarArgsSQLFunction(string.Empty, " - ", string.Empty);

            var temp = _session.QueryOver<SalesOrderDetail>().SelectList(list => list
                        .Select(Projections.SqlGroupProjection("SalesOrderID as OrderNumber", "SalesOrderID", new[] { "OrderNumber" },
                                new IType[] { NHibernateUtil.Int32 })).WithAlias(() => result.OrderNumber)
                        .SelectCount(x => x.SalesOrderID).WithAlias(() => result.NumberOfOrderItems)
                        .SelectSum(x => x.LineTotal).WithAlias(() => result.SumOfOrderItems)
                        .SelectAvg(x => x.LineTotal).WithAlias(() => result.AverageOfOrderItemsDouble)
                        .SelectMax(x => x.LineTotal).WithAlias(() => result.MostExpensiveOrderItem)
                        .SelectMin(x => x.LineTotal).WithAlias(() => result.CheapestOrderItem)
                        .Select(Projections.SqlFunction(subtractFunction, NHibernateUtil.Decimal, Projections.Max("LineTotal"), Projections.Min("LineTotal"))).WithAlias(() => result.RangeOfOrderItems))
                        //.Select(Projections.SqlFunction("stdev", NHibernateUtil.Double, Projections.Property("LineTotal"))).WithAlias(() => result.DeviationFromAverage))
                .OrderByAlias(() => result.NumberOfOrderItems).Desc
             .TransformUsing(Transformers.AliasToBean<GroupedOrderItemWithStatisticsViewModel>())
             .List<GroupedOrderItemWithStatisticsViewModel>()
             .ToList();

            return temp;
        }
        public void GetTransactionProductsWithInformation()
        {
            TransactionProductsWithInformationViewModel result = null;
            Product productAlias = null;
            TransactionHistory transactionHistoryAlias = null;
            ProductProductPhoto productProductPhotoAlias = null;
            ProductPhoto productPhotoAlias = null;
            ProductModel productModelAlias = null;
            ProductModelProductDescriptionCulture productModelProductDescriptionCultureAlias = null;
            ProductDescription productDescriptionAlias = null;

            _session.QueryOver<Product>(() => productAlias)
                .JoinQueryOver<TransactionHistory>(c => c.TransactionHistories, () => transactionHistoryAlias)
                .JoinQueryOver<ProductProductPhoto>(() => productAlias.ProductProductPhotos, () => productProductPhotoAlias)
                .JoinQueryOver<ProductPhoto>(() => productProductPhotoAlias.ProductPhoto, () => productPhotoAlias)
                .JoinQueryOver<ProductModel>(() => productAlias.ProductModel, () => productModelAlias)
                .JoinQueryOver<ProductModelProductDescriptionCulture>(() => productModelAlias.ProductModelProductDescriptionCultures, () => productModelProductDescriptionCultureAlias).Where(Restrictions.Eq("CultureID","en"))
                .JoinQueryOver<ProductDescription>(() => productModelProductDescriptionCultureAlias.ProductDescription, () => productDescriptionAlias)
                .SelectList( list => list
                    .Select(p => productAlias.ProductID).WithAlias(() => result.ProductID)
                    .Select(p => transactionHistoryAlias.TransactionID).WithAlias(() => result.TransactionID)
                    .Select(p => productPhotoAlias.ThumbNailPhoto).WithAlias(() => result.ProductPhoto)
                    .Select(p => productDescriptionAlias.Description).WithAlias(() => result.ProductDescription))
             .OrderByAlias(() => result.TransactionID).Desc
             .TransformUsing(Transformers.AliasToBean<TransactionProductsWithInformationViewModel>())
             .List<TransactionProductsWithInformationViewModel>()
             .ToList();
        }
    }
}
