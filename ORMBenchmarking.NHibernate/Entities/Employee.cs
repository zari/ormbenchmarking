﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Employee information such as salary, department, and title.
    /// </summary>
    public partial class Employee {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Employee constructor in the schema.
        /// </summary>
        public Employee()
        {
            this.SalariedFlag = true;
            this.VacationHours = 0;
            this.SickLeaveHours = 0;
            this.CurrentFlag = true;
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            this.EmployeeDepartmentHistories = new HashSet<EmployeeDepartmentHistory>();
            this.EmployeePayHistories = new HashSet<EmployeePayHistory>();
            this.JobCandidates = new HashSet<JobCandidate>();
            this.Documents = new HashSet<Document>();
            this.PurchaseOrderHeaders = new HashSet<PurchaseOrderHeader>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for Employee records.  Foreign key to BusinessEntity.BusinessEntityID.
        /// </summary>
        public virtual int BusinessEntityID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Unique national identification number such as a social security number.
        /// </summary>
        public virtual string NationalIDNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// Network login.
        /// </summary>
        public virtual string LoginID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Where the employee is located in corporate hierarchy.
        /// </summary>
        public virtual string OrganizationNode
        {
            get;
            set;
        }

    
        /// <summary>
        /// The depth of the employee in the corporate hierarchy.
        /// </summary>
        public virtual short? OrganizationLevel
        {
            get;
            set;
        }

    
        /// <summary>
        /// Work title such as Buyer or Sales Representative.
        /// </summary>
        public virtual string JobTitle
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date of birth.
        /// </summary>
        public virtual System.DateTime BirthDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// M = Married, S = Single
        /// </summary>
        public virtual string MaritalStatus
        {
            get;
            set;
        }

    
        /// <summary>
        /// M = Male, F = Female
        /// </summary>
        public virtual string Gender
        {
            get;
            set;
        }

    
        /// <summary>
        /// Employee hired on this date.
        /// </summary>
        public virtual System.DateTime HireDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// Job classification. 0 = Hourly, not exempt from collective bargaining. 1 = Salaried, exempt from collective bargaining.
        /// </summary>
        public virtual bool SalariedFlag
        {
            get;
            set;
        }

    
        /// <summary>
        /// Number of available vacation hours.
        /// </summary>
        public virtual short VacationHours
        {
            get;
            set;
        }

    
        /// <summary>
        /// Number of available sick leave hours.
        /// </summary>
        public virtual short SickLeaveHours
        {
            get;
            set;
        }

    
        /// <summary>
        /// 0 = Inactive, 1 = Active
        /// </summary>
        public virtual bool CurrentFlag
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Person in the schema.
        /// </summary>
        public virtual Person Person
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for EmployeeDepartmentHistories in the schema.
        /// </summary>
        public virtual ISet<EmployeeDepartmentHistory> EmployeeDepartmentHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for EmployeePayHistories in the schema.
        /// </summary>
        public virtual ISet<EmployeePayHistory> EmployeePayHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for JobCandidates in the schema.
        /// </summary>
        public virtual ISet<JobCandidate> JobCandidates
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Documents in the schema.
        /// </summary>
        public virtual ISet<Document> Documents
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PurchaseOrderHeaders in the schema.
        /// </summary>
        public virtual ISet<PurchaseOrderHeader> PurchaseOrderHeaders
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesPerson in the schema.
        /// </summary>
        public virtual SalesPerson SalesPerson
        {
            get;
            set;
        }
    }

}
