﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Customers (resellers) of Adventure Works products.
    /// </summary>
    public partial class Store {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Store constructor in the schema.
        /// </summary>
        public Store()
        {
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            this.Customers = new HashSet<Customer>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key. Foreign key to Customer.BusinessEntityID.
        /// </summary>
        public virtual int BusinessEntityID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Name of the store.
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

    
        /// <summary>
        /// Demographic informationg about the store such as the number of employees, annual sales and store type.
        /// </summary>
        public virtual string Demographics
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Customers in the schema.
        /// </summary>
        public virtual ISet<Customer> Customers
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesPerson in the schema.
        /// </summary>
        public virtual SalesPerson SalesPerson
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for BusinessEntity in the schema.
        /// </summary>
        public virtual BusinessEntity BusinessEntity
        {
            get;
            set;
        }
    }

}
