﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Sales territory lookup table.
    /// </summary>
    public partial class SalesTerritory {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for SalesTerritory constructor in the schema.
        /// </summary>
        public SalesTerritory()
        {
            this.SalesYTD = 0.00m;
            this.SalesLastYear = 0.00m;
            this.CostYTD = 0.00m;
            this.CostLastYear = 0.00m;
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            this.StateProvinces = new HashSet<StateProvince>();
            this.Customers = new HashSet<Customer>();
            this.SalesOrderHeaders = new HashSet<SalesOrderHeader>();
            this.SalesPeople = new HashSet<SalesPerson>();
            this.SalesTerritoryHistories = new HashSet<SalesTerritoryHistory>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for SalesTerritory records.
        /// </summary>
        public virtual int TerritoryID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Sales territory description
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

    
        /// <summary>
        /// Geographic area to which the sales territory belong.
        /// </summary>
        public virtual string Group
        {
            get;
            set;
        }

    
        /// <summary>
        /// Sales in the territory year to date.
        /// </summary>
        public virtual decimal SalesYTD
        {
            get;
            set;
        }

    
        /// <summary>
        /// Sales in the territory the previous year.
        /// </summary>
        public virtual decimal SalesLastYear
        {
            get;
            set;
        }

    
        /// <summary>
        /// Business costs in the territory year to date.
        /// </summary>
        public virtual decimal CostYTD
        {
            get;
            set;
        }

    
        /// <summary>
        /// Business costs in the territory the previous year.
        /// </summary>
        public virtual decimal CostLastYear
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for StateProvinces in the schema.
        /// </summary>
        public virtual ISet<StateProvince> StateProvinces
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Customers in the schema.
        /// </summary>
        public virtual ISet<Customer> Customers
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesOrderHeaders in the schema.
        /// </summary>
        public virtual ISet<SalesOrderHeader> SalesOrderHeaders
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesPeople in the schema.
        /// </summary>
        public virtual ISet<SalesPerson> SalesPeople
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for CountryRegion in the schema.
        /// </summary>
        public virtual CountryRegion CountryRegion
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesTerritoryHistories in the schema.
        /// </summary>
        public virtual ISet<SalesTerritoryHistory> SalesTerritoryHistories
        {
            get;
            set;
        }
    }

}
