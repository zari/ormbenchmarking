﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Products sold or used in the manfacturing of sold products.
    /// </summary>
    public partial class Product {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Product constructor in the schema.
        /// </summary>
        public Product()
        {
            this.MakeFlag = true;
            this.FinishedGoodsFlag = true;
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            this.BillOfMaterials_ProductAssemblyID = new HashSet<BillOfMaterial>();
            this.BillOfMaterials_ComponentID = new HashSet<BillOfMaterial>();
            this.ProductCostHistories = new HashSet<ProductCostHistory>();
            this.ProductDocuments = new HashSet<ProductDocument>();
            this.ProductInventories = new HashSet<ProductInventory>();
            this.ProductListPriceHistories = new HashSet<ProductListPriceHistory>();
            this.ProductProductPhotos = new HashSet<ProductProductPhoto>();
            this.ProductReviews = new HashSet<ProductReview>();
            this.TransactionHistories = new HashSet<TransactionHistory>();
            this.WorkOrders = new HashSet<WorkOrder>();
            this.ProductVendors = new HashSet<ProductVendor>();
            this.PurchaseOrderDetails = new HashSet<PurchaseOrderDetail>();
            this.ShoppingCartItems = new HashSet<ShoppingCartItem>();
            this.SpecialOfferProducts = new HashSet<SpecialOfferProduct>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for Product records.
        /// </summary>
        public virtual int ProductID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Name of the product.
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

    
        /// <summary>
        /// Unique product identification number.
        /// </summary>
        public virtual string ProductNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// 0 = Product is purchased, 1 = Product is manufactured in-house.
        /// </summary>
        public virtual bool MakeFlag
        {
            get;
            set;
        }

    
        /// <summary>
        /// 0 = Product is not a salable item. 1 = Product is salable.
        /// </summary>
        public virtual bool FinishedGoodsFlag
        {
            get;
            set;
        }

    
        /// <summary>
        /// Product color.
        /// </summary>
        public virtual string Color
        {
            get;
            set;
        }

    
        /// <summary>
        /// Minimum inventory quantity. 
        /// </summary>
        public virtual short SafetyStockLevel
        {
            get;
            set;
        }

    
        /// <summary>
        /// Inventory level that triggers a purchase order or work order. 
        /// </summary>
        public virtual short ReorderPoint
        {
            get;
            set;
        }

    
        /// <summary>
        /// Standard cost of the product.
        /// </summary>
        public virtual decimal StandardCost
        {
            get;
            set;
        }

    
        /// <summary>
        /// Selling price.
        /// </summary>
        public virtual decimal ListPrice
        {
            get;
            set;
        }

    
        /// <summary>
        /// Product size.
        /// </summary>
        public virtual string Size
        {
            get;
            set;
        }

    
        /// <summary>
        /// Product weight.
        /// </summary>
        public virtual decimal? Weight
        {
            get;
            set;
        }

    
        /// <summary>
        /// Number of days required to manufacture the product.
        /// </summary>
        public virtual int DaysToManufacture
        {
            get;
            set;
        }

    
        /// <summary>
        /// R = Road, M = Mountain, T = Touring, S = Standard
        /// </summary>
        public virtual string ProductLine
        {
            get;
            set;
        }

    
        /// <summary>
        /// H = High, M = Medium, L = Low
        /// </summary>
        public virtual string Class
        {
            get;
            set;
        }

    
        /// <summary>
        /// W = Womens, M = Mens, U = Universal
        /// </summary>
        public virtual string Style
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date the product was available for sale.
        /// </summary>
        public virtual System.DateTime SellStartDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date the product was no longer available for sale.
        /// </summary>
        public virtual System.DateTime? SellEndDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date the product was discontinued.
        /// </summary>
        public virtual System.DateTime? DiscontinuedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for BillOfMaterials_ProductAssemblyID in the schema.
        /// </summary>
        public virtual ISet<BillOfMaterial> BillOfMaterials_ProductAssemblyID
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for BillOfMaterials_ComponentID in the schema.
        /// </summary>
        public virtual ISet<BillOfMaterial> BillOfMaterials_ComponentID
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for UnitMeasure_SizeUnitMeasureCode in the schema.
        /// </summary>
        public virtual UnitMeasure UnitMeasure_SizeUnitMeasureCode
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for UnitMeasure_WeightUnitMeasureCode in the schema.
        /// </summary>
        public virtual UnitMeasure UnitMeasure_WeightUnitMeasureCode
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductModel in the schema.
        /// </summary>
        public virtual ProductModel ProductModel
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductSubcategory in the schema.
        /// </summary>
        public virtual ProductSubcategory ProductSubcategory
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductCostHistories in the schema.
        /// </summary>
        public virtual ISet<ProductCostHistory> ProductCostHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductDocuments in the schema.
        /// </summary>
        public virtual ISet<ProductDocument> ProductDocuments
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductInventories in the schema.
        /// </summary>
        public virtual ISet<ProductInventory> ProductInventories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductListPriceHistories in the schema.
        /// </summary>
        public virtual ISet<ProductListPriceHistory> ProductListPriceHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductProductPhotos in the schema.
        /// </summary>
        public virtual ISet<ProductProductPhoto> ProductProductPhotos
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductReviews in the schema.
        /// </summary>
        public virtual ISet<ProductReview> ProductReviews
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for TransactionHistories in the schema.
        /// </summary>
        public virtual ISet<TransactionHistory> TransactionHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for WorkOrders in the schema.
        /// </summary>
        public virtual ISet<WorkOrder> WorkOrders
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductVendors in the schema.
        /// </summary>
        public virtual ISet<ProductVendor> ProductVendors
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PurchaseOrderDetails in the schema.
        /// </summary>
        public virtual ISet<PurchaseOrderDetail> PurchaseOrderDetails
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ShoppingCartItems in the schema.
        /// </summary>
        public virtual ISet<ShoppingCartItem> ShoppingCartItems
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SpecialOfferProducts in the schema.
        /// </summary>
        public virtual ISet<SpecialOfferProduct> SpecialOfferProducts
        {
            get;
            set;
        }
    }

}
