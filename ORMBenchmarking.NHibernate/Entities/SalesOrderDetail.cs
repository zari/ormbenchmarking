﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Individual products associated with a specific sales order. See SalesOrderHeader.
    /// </summary>
    public partial class SalesOrderDetail {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();

        public override bool Equals(object obj)
        {
          SalesOrderDetail toCompare = obj as SalesOrderDetail;
          if (toCompare == null)
          {
            return false;
          }

          if (!Object.Equals(this.SalesOrderID, toCompare.SalesOrderID))
            return false;
          if (!Object.Equals(this.SalesOrderDetailID, toCompare.SalesOrderDetailID))
            return false;
          
          return true;
        }

        public override int GetHashCode()
        {
          int hashCode = 13;
          hashCode = (hashCode * 7) + SalesOrderID.GetHashCode();
          hashCode = (hashCode * 7) + SalesOrderDetailID.GetHashCode();
          return hashCode;
        }
        
        #endregion
        /// <summary>
        /// There are no comments for SalesOrderDetail constructor in the schema.
        /// </summary>
        public SalesOrderDetail()
        {
            this.UnitPriceDiscount = 0.0m;
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            OnCreated();
        }

    
        /// <summary>
        /// Primary key. Foreign key to SalesOrderHeader.SalesOrderID.
        /// </summary>
        public virtual int SalesOrderID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Primary key. One incremental unique number per product sold.
        /// </summary>
        public virtual int SalesOrderDetailID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Shipment tracking number supplied by the shipper.
        /// </summary>
        public virtual string CarrierTrackingNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// Quantity ordered per product.
        /// </summary>
        public virtual short OrderQty
        {
            get;
            set;
        }

    
        /// <summary>
        /// Selling price of a single product.
        /// </summary>
        public virtual decimal UnitPrice
        {
            get;
            set;
        }

    
        /// <summary>
        /// Discount amount.
        /// </summary>
        public virtual decimal UnitPriceDiscount
        {
            get;
            set;
        }

    
        /// <summary>
        /// Per product subtotal. Computed as UnitPrice * (1 - UnitPriceDiscount) * OrderQty.
        /// </summary>
        public virtual decimal LineTotal
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesOrderHeader in the schema.
        /// </summary>
        public virtual SalesOrderHeader SalesOrderHeader
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SpecialOfferProduct in the schema.
        /// </summary>
        public virtual SpecialOfferProduct SpecialOfferProduct
        {
            get;
            set;
        }

        /// <summary>
        /// There are no comments for Product in the schema.
        /// </summary>
        public virtual Product Product
        {
            get;
            set;
        }
    }

}
