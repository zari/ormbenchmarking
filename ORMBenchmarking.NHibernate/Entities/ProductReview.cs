﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Customer reviews of products they have purchased.
    /// </summary>
    public partial class ProductReview {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for ProductReview constructor in the schema.
        /// </summary>
        public ProductReview()
        {
            this.ReviewDate = DateTime.Now;
            this.ModifiedDate = DateTime.Now;
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for ProductReview records.
        /// </summary>
        public virtual int ProductReviewID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Name of the reviewer.
        /// </summary>
        public virtual string ReviewerName
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date review was submitted.
        /// </summary>
        public virtual System.DateTime ReviewDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// Reviewer&apos;s e-mail address.
        /// </summary>
        public virtual string EmailAddress
        {
            get;
            set;
        }

    
        /// <summary>
        /// Product rating given by the reviewer. Scale is 1 to 5 with 5 as the highest rating.
        /// </summary>
        public virtual int Rating
        {
            get;
            set;
        }

    
        /// <summary>
        /// Reviewer&apos;s comments
        /// </summary>
        public virtual string Comments
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Product in the schema.
        /// </summary>
        public virtual Product Product
        {
            get;
            set;
        }
    }

}
