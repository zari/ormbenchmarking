﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Individual products associated with a specific purchase order. See PurchaseOrderHeader.
    /// </summary>
    public partial class PurchaseOrderDetail {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();

        public override bool Equals(object obj)
        {
          PurchaseOrderDetail toCompare = obj as PurchaseOrderDetail;
          if (toCompare == null)
          {
            return false;
          }

          if (!Object.Equals(this.PurchaseOrderID, toCompare.PurchaseOrderID))
            return false;
          if (!Object.Equals(this.PurchaseOrderDetailID, toCompare.PurchaseOrderDetailID))
            return false;
          
          return true;
        }

        public override int GetHashCode()
        {
          int hashCode = 13;
          hashCode = (hashCode * 7) + PurchaseOrderID.GetHashCode();
          hashCode = (hashCode * 7) + PurchaseOrderDetailID.GetHashCode();
          return hashCode;
        }
        
        #endregion
        /// <summary>
        /// There are no comments for PurchaseOrderDetail constructor in the schema.
        /// </summary>
        public PurchaseOrderDetail()
        {
            this.ModifiedDate = DateTime.Now;
            OnCreated();
        }

    
        /// <summary>
        /// Primary key. Foreign key to PurchaseOrderHeader.PurchaseOrderID.
        /// </summary>
        public virtual int PurchaseOrderID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Primary key. One line number per purchased product.
        /// </summary>
        public virtual int PurchaseOrderDetailID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date the product is expected to be received.
        /// </summary>
        public virtual System.DateTime DueDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// Quantity ordered.
        /// </summary>
        public virtual short OrderQty
        {
            get;
            set;
        }

    
        /// <summary>
        /// Vendor&apos;s selling price of a single product.
        /// </summary>
        public virtual decimal UnitPrice
        {
            get;
            set;
        }

    
        /// <summary>
        /// Per product subtotal. Computed as OrderQty * UnitPrice.
        /// </summary>
        public virtual decimal LineTotal
        {
            get;
            set;
        }

    
        /// <summary>
        /// Quantity actually received from the vendor.
        /// </summary>
        public virtual decimal ReceivedQty
        {
            get;
            set;
        }

    
        /// <summary>
        /// Quantity rejected during inspection.
        /// </summary>
        public virtual decimal RejectedQty
        {
            get;
            set;
        }

    
        /// <summary>
        /// Quantity accepted into inventory. Computed as ReceivedQty - RejectedQty.
        /// </summary>
        public virtual decimal StockedQty
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PurchaseOrderHeader in the schema.
        /// </summary>
        public virtual PurchaseOrderHeader PurchaseOrderHeader
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Product in the schema.
        /// </summary>
        public virtual Product Product
        {
            get;
            set;
        }
    }

}
