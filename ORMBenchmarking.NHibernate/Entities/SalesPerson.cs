﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Sales representative current information.
    /// </summary>
    public partial class SalesPerson {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for SalesPerson constructor in the schema.
        /// </summary>
        public SalesPerson()
        {
            this.Bonus = 0.00m;
            this.CommissionPct = 0.00m;
            this.SalesYTD = 0.00m;
            this.SalesLastYear = 0.00m;
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            this.SalesOrderHeaders = new HashSet<SalesOrderHeader>();
            this.SalesPersonQuotaHistories = new HashSet<SalesPersonQuotaHistory>();
            this.SalesTerritoryHistories = new HashSet<SalesTerritoryHistory>();
            this.Stores = new HashSet<Store>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for SalesPerson records. Foreign key to Employee.BusinessEntityID
        /// </summary>
        public virtual int BusinessEntityID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Projected yearly sales.
        /// </summary>
        public virtual decimal? SalesQuota
        {
            get;
            set;
        }

    
        /// <summary>
        /// Bonus due if quota is met.
        /// </summary>
        public virtual decimal Bonus
        {
            get;
            set;
        }

    
        /// <summary>
        /// Commision percent received per sale.
        /// </summary>
        public virtual decimal CommissionPct
        {
            get;
            set;
        }

    
        /// <summary>
        /// Sales total year to date.
        /// </summary>
        public virtual decimal SalesYTD
        {
            get;
            set;
        }

    
        /// <summary>
        /// Sales total of previous year.
        /// </summary>
        public virtual decimal SalesLastYear
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesOrderHeaders in the schema.
        /// </summary>
        public virtual ISet<SalesOrderHeader> SalesOrderHeaders
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesTerritory in the schema.
        /// </summary>
        public virtual SalesTerritory SalesTerritory
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Employee in the schema.
        /// </summary>
        public virtual Employee Employee
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesPersonQuotaHistories in the schema.
        /// </summary>
        public virtual ISet<SalesPersonQuotaHistory> SalesPersonQuotaHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesTerritoryHistories in the schema.
        /// </summary>
        public virtual ISet<SalesTerritoryHistory> SalesTerritoryHistories
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Stores in the schema.
        /// </summary>
        public virtual ISet<Store> Stores
        {
            get;
            set;
        }
    }

}
