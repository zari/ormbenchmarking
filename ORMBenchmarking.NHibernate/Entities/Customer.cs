﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Current customer information. Also see the Person and Store tables.
    /// </summary>
    public partial class Customer {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for Customer constructor in the schema.
        /// </summary>
        public Customer()
        {
            this.Rowguid = new Guid();
            this.ModifiedDate = DateTime.Now;
            this.SalesOrderHeaders = new HashSet<SalesOrderHeader>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key.
        /// </summary>
        public virtual int CustomerID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Unique number identifying the customer assigned by the accounting system.
        /// </summary>
        public virtual string AccountNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// ROWGUIDCOL number uniquely identifying the record. Used to support a merge replication sample.
        /// </summary>
        public virtual System.Guid Rowguid
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Store in the schema.
        /// </summary>
        public virtual Store Store
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesTerritory in the schema.
        /// </summary>
        public virtual SalesTerritory SalesTerritory
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Person in the schema.
        /// </summary>
        public virtual Person Person
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for SalesOrderHeaders in the schema.
        /// </summary>
        public virtual ISet<SalesOrderHeader> SalesOrderHeaders
        {
            get;
            set;
        }
    }

}
