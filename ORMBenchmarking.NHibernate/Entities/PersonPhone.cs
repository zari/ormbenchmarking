﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Telephone number and type of a person.
    /// </summary>
    public partial class PersonPhone {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();

        public override bool Equals(object obj)
        {
          PersonPhone toCompare = obj as PersonPhone;
          if (toCompare == null)
          {
            return false;
          }

          if (!Object.Equals(this.BusinessEntityID, toCompare.BusinessEntityID))
            return false;
          if (!Object.Equals(this.PhoneNumber, toCompare.PhoneNumber))
            return false;
          if (!Object.Equals(this.PhoneNumberTypeID, toCompare.PhoneNumberTypeID))
            return false;
          
          return true;
        }

        public override int GetHashCode()
        {
          int hashCode = 13;
          hashCode = (hashCode * 7) + BusinessEntityID.GetHashCode();
          hashCode = (hashCode * 7) + PhoneNumber.GetHashCode();
          hashCode = (hashCode * 7) + PhoneNumberTypeID.GetHashCode();
          return hashCode;
        }
        
        #endregion
        /// <summary>
        /// There are no comments for PersonPhone constructor in the schema.
        /// </summary>
        public PersonPhone()
        {
            this.ModifiedDate = DateTime.Now;
            OnCreated();
        }

    
        /// <summary>
        /// Business entity identification number. Foreign key to Person.BusinessEntityID.
        /// </summary>
        public virtual int BusinessEntityID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Telephone number identification number.
        /// </summary>
        public virtual string PhoneNumber
        {
            get;
            set;
        }

    
        /// <summary>
        /// Kind of phone number. Foreign key to PhoneNumberType.PhoneNumberTypeID.
        /// </summary>
        public virtual int PhoneNumberTypeID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Person in the schema.
        /// </summary>
        public virtual Person Person
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for PhoneNumberType in the schema.
        /// </summary>
        public virtual PhoneNumberType PhoneNumberType
        {
            get;
            set;
        }
    }

}
