﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Product images.
    /// </summary>
    public partial class ProductPhoto {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for ProductPhoto constructor in the schema.
        /// </summary>
        public ProductPhoto()
        {
            this.ModifiedDate = DateTime.Now;
            this.ProductProductPhotos = new HashSet<ProductProductPhoto>();
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for ProductPhoto records.
        /// </summary>
        public virtual int ProductPhotoID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Small image of the product.
        /// </summary>
        public virtual byte[] ThumbNailPhoto
        {
            get;
            set;
        }

    
        /// <summary>
        /// Small image file name.
        /// </summary>
        public virtual string ThumbnailPhotoFileName
        {
            get;
            set;
        }

    
        /// <summary>
        /// Large image of the product.
        /// </summary>
        public virtual byte[] LargePhoto
        {
            get;
            set;
        }

    
        /// <summary>
        /// Large image file name.
        /// </summary>
        public virtual string LargePhotoFileName
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for ProductProductPhotos in the schema.
        /// </summary>
        public virtual ISet<ProductProductPhoto> ProductProductPhotos
        {
            get;
            set;
        }
    }

}
