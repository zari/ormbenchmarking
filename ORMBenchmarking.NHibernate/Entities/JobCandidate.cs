﻿//------------------------------------------------------------------------------
// This is auto-generated code.
//------------------------------------------------------------------------------
// This code was generated by Entity Developer tool using NHibernate template.
// Code is generated on: 26.11.2018 21:55:24
//
// Changes to this file may cause incorrect behavior and will be lost if
// the code is regenerated.
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace ORMBenchmarking.NHibernate.Entities
{

    /// <summary>
    /// Résumés submitted to Human Resources by job applicants.
    /// </summary>
    public partial class JobCandidate {
    
        #region Extensibility Method Definitions
        
        /// <summary>
        /// There are no comments for OnCreated in the schema.
        /// </summary>
        partial void OnCreated();
        
        #endregion
        /// <summary>
        /// There are no comments for JobCandidate constructor in the schema.
        /// </summary>
        public JobCandidate()
        {
            this.ModifiedDate = DateTime.Now;
            OnCreated();
        }

    
        /// <summary>
        /// Primary key for JobCandidate records.
        /// </summary>
        public virtual int JobCandidateID
        {
            get;
            set;
        }

    
        /// <summary>
        /// Résumé in XML format.
        /// </summary>
        public virtual string Resume
        {
            get;
            set;
        }

    
        /// <summary>
        /// Date and time the record was last updated.
        /// </summary>
        public virtual System.DateTime ModifiedDate
        {
            get;
            set;
        }

    
        /// <summary>
        /// There are no comments for Employee in the schema.
        /// </summary>
        public virtual Employee Employee
        {
            get;
            set;
        }
    }

}
