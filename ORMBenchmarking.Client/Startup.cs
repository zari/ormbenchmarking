﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ORMBenchmarking.AdoNet.Abstract;
using ORMBenchmarking.AdoNet.Repository;
using ORMBenchmarking.Dapper.Abstract;
using ORMBenchmarking.Dapper.Repository;
using ORMBenchmarking.EntityFramework.Abstract;
using ORMBenchmarking.EntityFramework.Repository;
using ORMBenchmarking.NHibernate.Abstract;
using ORMBenchmarking.NHibernate.Repository;

namespace ORMBenchmarking.Client
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddTransient<IEntityRepository, EntityRepository>();
            services.AddTransient<INHibernateRepository, NHibernateRepository>();
            services.AddTransient<IDapperRepository, DapperRepository>();
            services.AddTransient<IAdoNetRepository, AdoNetRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Benchmark/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Benchmark}/{action=Index}/{id?}");
            });
        }
    }
}
