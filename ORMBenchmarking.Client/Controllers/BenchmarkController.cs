﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ORMBenchmarking.AdoNet.Abstract;
using ORMBenchmarking.Client.Models;
using ORMBenchmarking.Dapper.Abstract;
using ORMBenchmarking.EntityFramework.Abstract;
using ORMBenchmarking.Models.Helpers;
using ORMBenchmarking.Models.ViewModel;
using ORMBenchmarking.NHibernate.Abstract;
using static ORMBenchmarking.Models.Helpers.Enums;

namespace ORMBenchmarking.Client.Controllers
{
    public class BenchmarkController : Controller
    {
        private int _queryQuantity = 10;
        private IEntityRepository _entityRepo;
        private INHibernateRepository _nHibernateRepo;
        private IDapperRepository _dapperRepo;
        private IAdoNetRepository _adoNetRepo;

        public BenchmarkController(IEntityRepository entityRepo, INHibernateRepository nHibernateRepo, IDapperRepository dapperRepo, IAdoNetRepository adoNetRepo)
        {
            _entityRepo = entityRepo;
            _nHibernateRepo = nHibernateRepo;
            _dapperRepo = dapperRepo;
            _adoNetRepo = adoNetRepo;
        }

        public IActionResult Index()
        {
           return View();
        }

        public IActionResult Info()
        {
            ViewData["Message"] = "Informacje o aplikacji.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult ExecuteSelectedQuery()
        {
            if (Enum.TryParse(Request.Form["queryType"], out QueryType castedQueryType))
            {
                var executionTimeResults = new Dictionary<LibraryType, List<double>>();

                executionTimeResults.Add(LibraryType.EntityFramework, GetExecutionTimeForEntity(castedQueryType));
                executionTimeResults.Add(LibraryType.NHibernate, GetExecutionTimeForNHibernate(castedQueryType));
                executionTimeResults.Add(LibraryType.Dapper, GetExecutionTimeForDapper(castedQueryType));
                executionTimeResults.Add(LibraryType.Adonet, GetExecutionTimeForAdonet(castedQueryType));

                ExecutionTimeViewModel executionTimeVM = new ExecutionTimeViewModel(executionTimeResults, castedQueryType);

                return View("QueryTimeResult", executionTimeVM);
            }
            return View("Index");
        }

        private List<double> GetExecutionTimeForEntity(QueryType qType)
        {
            List<double> results = new List<double>();
            Stopwatch st = new Stopwatch();
            for (int i = 0; i < _queryQuantity; i++)
            {
                st.Reset();
                st.Start();
                GetQueryToExecute(LibraryType.EntityFramework, qType);
                st.Stop();
                results.Add(st.Elapsed.TotalSeconds);
                GC.Collect();

            }
            return results;
        }

        private List<double> GetExecutionTimeForNHibernate(QueryType qType)
        {
            List<double> results = new List<double>();
            Stopwatch st = new Stopwatch();
            for (int i = 0; i < _queryQuantity; i++)
            {
                st.Reset();
                st.Start();
                GetQueryToExecute(LibraryType.NHibernate, qType);
                st.Stop();
                results.Add(st.Elapsed.TotalSeconds);
                GC.Collect();

            }
            return results;
        }

        private List<double> GetExecutionTimeForDapper(QueryType qType)
        {
            List<double> results = new List<double>();
            Stopwatch st = new Stopwatch();
            for (int i = 0; i < _queryQuantity; i++)
            {
                st.Reset();
                st.Start();
                GetQueryToExecute(LibraryType.Dapper, qType);
                st.Stop();
                results.Add(st.Elapsed.TotalSeconds);
                GC.Collect();

            }
            return results;
        }

        private List<double> GetExecutionTimeForAdonet(QueryType qType)
        {
            List<double> results = new List<double>();
            Stopwatch st = new Stopwatch();
            for (int i = 0; i < _queryQuantity; i++)
            {
                st.Reset();
                st.Start();
                GetQueryToExecute(LibraryType.Adonet, qType);
                st.Stop();
                results.Add(st.Elapsed.TotalSeconds);
            }

            return results;
        }

        private dynamic GetQueryToExecute(LibraryType lType, QueryType qType)
        {
            switch (lType)
            {
                case LibraryType.EntityFramework:
                    switch (qType)
                    {
                        case QueryType.Query_1:
                            return _entityRepo.GetAllTransactions();
                        case QueryType.Query_2:
                            return _entityRepo.GetAllArchiveTransactions();
                        case QueryType.Query_3:
                            return _entityRepo.GetSumOfAllTransactions();
                        case QueryType.Query_4:
                            return _entityRepo.GetEmployeesOlderThan50();
                        case QueryType.Query_5:
                            return _entityRepo.GetSalesByYears();
                        case QueryType.Query_6:
                            return _entityRepo.GetEmployeeQuantityPerDepartment();
                        case QueryType.Query_7:
                            return _entityRepo.GetSalesOrderDetailsWithProductInfo();
                        case QueryType.Query_8:
                            return _entityRepo.GetGroupedOrdersWithStatistics();
                        case QueryType.Query_9:
                            _entityRepo.GetTransactionProductsWithInformation();
                            return null;
                        default:
                            break;
                    }
                    break;
                case LibraryType.NHibernate:
                    switch (qType)
                    {
                        case QueryType.Query_1:
                            return _nHibernateRepo.GetAllTransactions();
                        case QueryType.Query_2:
                            return _nHibernateRepo.GetAllArchiveTransactions();
                        case QueryType.Query_3:
                            return _nHibernateRepo.GetSumOfAllTransactions();
                        case QueryType.Query_4:
                            return _nHibernateRepo.GetEmployeesOlderThan50();
                        case QueryType.Query_5:
                            return _nHibernateRepo.GetSalesByYears();
                        case QueryType.Query_6:
                            return _nHibernateRepo.GetEmployeeQuantityPerDepartment();
                        case QueryType.Query_7:
                            return _nHibernateRepo.GetSalesOrderDetailsWithProductInfo();
                        case QueryType.Query_8:
                            return _nHibernateRepo.GetGroupedOrdersWithStatistics();
                        case QueryType.Query_9:
                            _nHibernateRepo.GetTransactionProductsWithInformation();
                            return null;
                        default:
                            break;
                    }
                    break;
                case LibraryType.Dapper:
                    switch (qType)
                    {
                        case QueryType.Query_1:
                            return _dapperRepo.GetAllTransactions();
                        case QueryType.Query_2:
                            return _dapperRepo.GetAllArchiveTransactions();
                        case QueryType.Query_3:
                            return _dapperRepo.GetSumOfAllTransactions();
                        case QueryType.Query_4:
                            return _dapperRepo.GetEmployeesOlderThan50();
                        case QueryType.Query_5:
                            return _dapperRepo.GetSalesByYears();
                        case QueryType.Query_6:
                            return _dapperRepo.GetEmployeeQuantityPerDepartment();
                        case QueryType.Query_7:
                            return _dapperRepo.GetSalesOrderDetailsWithProductInfo();
                        case QueryType.Query_8:
                            return _dapperRepo.GetGroupedOrdersWithStatistics();
                        case QueryType.Query_9:
                             _dapperRepo.GetTransactionProductsWithInformation();
                             return null;
                        default:
                            break;
                    }
                    break;
                case LibraryType.Adonet:
                    switch (qType)
                    {
                        case QueryType.Query_1:
                            return _adoNetRepo.GetAllTransactions();
                        case QueryType.Query_2:
                            return _adoNetRepo.GetAllArchiveTransactions();
                        case QueryType.Query_3:
                            return _adoNetRepo.GetSumOfAllTransactions();
                        case QueryType.Query_4:
                            return _adoNetRepo.GetEmployeesOlderThan50();
                        case QueryType.Query_5:
                            return _adoNetRepo.GetSalesByYears();
                        case QueryType.Query_6:
                            return _adoNetRepo.GetEmployeeQuantityPerDepartment();
                        case QueryType.Query_7:
                            return _adoNetRepo.GetSalesOrderDetailsWithProductInfo();
                        case QueryType.Query_8:
                            return _adoNetRepo.GetGroupedOrdersWithStatistics();
                        case QueryType.Query_9:
                            _adoNetRepo.GetTransactionProductsWithInformation();
                            return null;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
