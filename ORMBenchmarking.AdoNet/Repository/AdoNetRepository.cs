﻿using ORMBenchmarking.AdoNet.Abstract;
using ORMBenchmarking.Models.Model;
using ORMBenchmarking.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ORMBenchmarking.AdoNet.Repository
{
    public class AdoNetRepository : IAdoNetRepository
    {
        private readonly string _connectionString = "Server=DESKTOP-H4PHU4K;Database=AdventureWorks2014;Trusted_Connection=True;";
        IDbConnection _con;

        public AdoNetRepository()
        {
        }

        public List<TransactionHistory> GetAllTransactions()
        {
            List<TransactionHistory> historyTransactions = new List<TransactionHistory>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = "select * from Production.TransactionHistory";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);
               
                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionHistory transaction = new TransactionHistory();

                    transaction.TransactionId = Convert.ToInt32(reader["TransactionId"]);
                    transaction.ActualCost = reader.GetDecimal(reader.GetOrdinal("ActualCost"));
                    transaction.ModifiedDate = reader.GetDateTime(reader.GetOrdinal("ModifiedDate"));
                    //transaction.Product = reader.GetDateTime(reader.GetOrdinal("ModifiedDate")); //without returning connected object
                    transaction.ProductId = reader.GetInt32(reader.GetOrdinal("ProductId"));
                    transaction.Quantity = reader.GetInt32(reader.GetOrdinal("Quantity"));
                    transaction.ReferenceOrderId = reader.GetInt32(reader.GetOrdinal("ReferenceOrderId"));
                    transaction.ReferenceOrderLineId = reader.GetInt32(reader.GetOrdinal("ReferenceOrderLineId"));
                    transaction.TransactionDate = reader.GetDateTime(reader.GetOrdinal("TransactionDate"));
                    transaction.TransactionType = reader.GetString(reader.GetOrdinal("TransactionType"));

                    historyTransactions.Add(transaction);
                }
                reader.Close();
            }
            return historyTransactions;
        }

        public List<TransactionHistoryArchive> GetAllArchiveTransactions()
        {
            List<TransactionHistoryArchive> historyArchiveTransactions = new List<TransactionHistoryArchive>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = "select * from Production.TransactionHistoryArchive";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionHistoryArchive archiveTransaction = new TransactionHistoryArchive();

                    archiveTransaction.TransactionId = Convert.ToInt32(reader["TransactionId"]);
                    archiveTransaction.ActualCost = reader.GetDecimal(reader.GetOrdinal("ActualCost"));
                    archiveTransaction.ModifiedDate = reader.GetDateTime(reader.GetOrdinal("ModifiedDate"));
                    archiveTransaction.ProductId = reader.GetInt32(reader.GetOrdinal("ProductId"));
                    archiveTransaction.Quantity = reader.GetInt32(reader.GetOrdinal("Quantity"));
                    archiveTransaction.ReferenceOrderId = reader.GetInt32(reader.GetOrdinal("ReferenceOrderId"));
                    archiveTransaction.ReferenceOrderLineId = reader.GetInt32(reader.GetOrdinal("ReferenceOrderLineId"));
                    archiveTransaction.TransactionDate = reader.GetDateTime(reader.GetOrdinal("TransactionDate"));
                    archiveTransaction.TransactionType = reader.GetString(reader.GetOrdinal("TransactionType"));

                    historyArchiveTransactions.Add(archiveTransaction);
                }
                reader.Close();
            }
            return historyArchiveTransactions;
        }

        public decimal GetSumOfAllTransactions()
        {
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = "select SUM(Quantity * ActualCost) from Production.TransactionHistory";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                return (decimal)command.ExecuteScalar();
            }
        }

        public List<EmployeeAgeViewModel> GetEmployeesOlderThan50()
        {
            List<EmployeeAgeViewModel> employeesOlderThan50 = new List<EmployeeAgeViewModel>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = @"select Person.LastName,
                        Person.FirstName,
                        Employee.Gender,
                        Employee.BirthDate,
                        Employee.HireDate,
                        DATEDIFF(YEAR, Employee.BirthDate, GETDATE()) as Age
                    FROM Person.Person
                    JOIN HumanResources.Employee
                        ON Person.BusinessEntityID = Employee.BusinessEntityID
                    WHERE Employee.BirthDate <= DATEADD(YEAR, -50, GETDATE()) 
                    ORDER BY Age DESC";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeAgeViewModel employeeAge = new EmployeeAgeViewModel();

                    employeeAge.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
                    employeeAge.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                    employeeAge.Gender = reader.GetString(reader.GetOrdinal("Gender"));
                    employeeAge.BirthDate = reader.GetDateTime(reader.GetOrdinal("BirthDate"));
                    employeeAge.HireDate = reader.GetDateTime(reader.GetOrdinal("HireDate"));
                    employeeAge.Age = reader.GetInt32(reader.GetOrdinal("Age"));

                    employeesOlderThan50.Add(employeeAge);
                }
                reader.Close();
            }
            return employeesOlderThan50;
        }

        public List<SalesYearViewModel> GetSalesByYears()
        {
            List<SalesYearViewModel> salesByYears = new List<SalesYearViewModel>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = @"select YEAR(OrderDate) AS YearOrder,
                        COUNT(*) AS OrderQuantity,
                        SUM(SubTotal) AS OrderPriceSum
                    FROM Sales.SalesOrderHeader  
                    GROUP BY YEAR(OrderDate)
                    ORDER BY YearOrder";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SalesYearViewModel saleYear = new SalesYearViewModel();

                    saleYear.YearOfSummary = reader.GetInt32(reader.GetOrdinal("YearOrder"));
                    saleYear.OrdersQuantity = reader.GetInt32(reader.GetOrdinal("OrderQuantity"));
                    saleYear.OrdersPriceSum = reader.GetDecimal(reader.GetOrdinal("OrderPriceSum"));

                    salesByYears.Add(saleYear);
                }
                reader.Close();
            }
            return salesByYears;
        }

        public List<EmployeeQuantityPerDepartmentViewModel> GetEmployeeQuantityPerDepartment()
        {
            List<EmployeeQuantityPerDepartmentViewModel> employeeQuantityInDepartments = new List<EmployeeQuantityPerDepartmentViewModel>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = @"SELECT HumanResources.Department.Name as DepartmentName,
	                    COUNT(*) as EmployeeQuantity
                    FROM HumanResources.EmployeeDepartmentHistory 
	                    JOIN HumanResources.Department
	                    ON HumanResources.EmployeeDepartmentHistory.DepartmentID = HumanResources.Department.DepartmentID
                    WHERE HumanResources.EmployeeDepartmentHistory.EndDate IS NULL
                    GROUP BY HumanResources.Department.Name
                    ORDER BY EmployeeQuantity DESC";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeQuantityPerDepartmentViewModel employeeQuantityInDepartment = new EmployeeQuantityPerDepartmentViewModel();

                    employeeQuantityInDepartment.DepartmentName = reader.GetString(reader.GetOrdinal("DepartmentName"));
                    employeeQuantityInDepartment.EmployeeQuantity = reader.GetInt32(reader.GetOrdinal("EmployeeQuantity"));

                    employeeQuantityInDepartments.Add(employeeQuantityInDepartment);
                }
                reader.Close();
            }
            return employeeQuantityInDepartments;
        }

        public List<SalesOrderWithProductInfoViewModel> GetSalesOrderDetailsWithProductInfo()
        {
            List<SalesOrderWithProductInfoViewModel> salesOrderDetailsWithProdcutInfo = new List<SalesOrderWithProductInfoViewModel>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = @"SELECT * FROM Sales.SalesOrderDetail s
                        INNER JOIN Production.Product p ON s.ProductID = p.ProductID
                        ORDER BY s.ProductID DESC, s.SalesOrderID";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);
                command.CommandTimeout = 300;
                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    SalesOrderWithProductInfoViewModel salesOrderDetail = new SalesOrderWithProductInfoViewModel();

                    salesOrderDetail.SalesOrderId = reader.GetInt32(reader.GetOrdinal("SalesOrderId"));
                    salesOrderDetail.SalesOrderDetailId = reader.GetInt32(reader.GetOrdinal("SalesOrderDetailId"));
                    salesOrderDetail.CarrierTrackingNumber = reader.IsDBNull(reader.GetOrdinal("CarrierTrackingNumber")) ? string.Empty : reader.GetString(reader.GetOrdinal("CarrierTrackingNumber"));
                    salesOrderDetail.OrderQty = reader.GetInt16(reader.GetOrdinal("OrderQty"));
                    salesOrderDetail.ProductId = reader.GetInt32(reader.GetOrdinal("ProductId"));
                    salesOrderDetail.SpecialOfferId = reader.GetInt32(reader.GetOrdinal("SpecialOfferId"));
                    salesOrderDetail.UnitPrice = reader.GetDecimal(reader.GetOrdinal("UnitPrice"));
                    salesOrderDetail.UnitPriceDiscount = reader.GetDecimal(reader.GetOrdinal("UnitPriceDiscount"));
                    salesOrderDetail.LineTotal = reader.GetDecimal(reader.GetOrdinal("LineTotal"));
                    salesOrderDetail.RowguidSalesOrder = reader.GetGuid(reader.GetOrdinal("Rowguid"));
                    salesOrderDetail.ModifiedDateSalesOrder = reader.GetDateTime(reader.GetOrdinal("ModifiedDate"));

                    salesOrderDetail.Name = reader.GetString(reader.GetOrdinal("Name"));
                    salesOrderDetail.ProductNumber = reader.GetString(reader.GetOrdinal("ProductNumber"));
                    salesOrderDetail.MakeFlag = reader.GetBoolean(reader.GetOrdinal("MakeFlag"));
                    salesOrderDetail.FinishedGoodsFlag = reader.GetBoolean(reader.GetOrdinal("FinishedGoodsFlag"));
                    salesOrderDetail.Color = reader.IsDBNull(reader.GetOrdinal("Color")) ? string.Empty : reader.GetString(reader.GetOrdinal("Color"));
                    salesOrderDetail.SafetyStockLevel = reader.GetInt16(reader.GetOrdinal("SafetyStockLevel"));
                    salesOrderDetail.ReorderPoint = reader.GetInt16(reader.GetOrdinal("ReorderPoint"));
                    salesOrderDetail.StandardCost = reader.GetDecimal(reader.GetOrdinal("StandardCost"));
                    salesOrderDetail.ListPrice = reader.GetDecimal(reader.GetOrdinal("ListPrice"));
                    salesOrderDetail.Size = reader.IsDBNull(reader.GetOrdinal("Size")) ? string.Empty : reader.GetString(reader.GetOrdinal("Size"));
                    salesOrderDetail.SizeUnitMeasureCode = reader.IsDBNull(reader.GetOrdinal("SizeUnitMeasureCode")) ? string.Empty : reader.GetString(reader.GetOrdinal("SizeUnitMeasureCode"));
                    salesOrderDetail.WeightUnitMeasureCode = reader.IsDBNull(reader.GetOrdinal("WeightUnitMeasureCode")) ? string.Empty : reader.GetString(reader.GetOrdinal("WeightUnitMeasureCode"));
                    salesOrderDetail.Weight = reader.IsDBNull(reader.GetOrdinal("WeightUnitMeasureCode")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Weight"));
                    salesOrderDetail.ProductSubcategoryId = reader.GetInt32(reader.GetOrdinal("ProductSubcategoryId"));
                    salesOrderDetail.ProductModelId = reader.GetInt32(reader.GetOrdinal("ProductModelId"));
                    salesOrderDetail.SellStartDate = reader.GetDateTime(reader.GetOrdinal("SellStartDate"));
                    salesOrderDetail.SellEndDate = reader.IsDBNull(reader.GetOrdinal("SellEndDate")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("SellEndDate"));
                    salesOrderDetail.DiscontinuedDate = reader.IsDBNull(reader.GetOrdinal("DiscontinuedDate")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("DiscontinuedDate"));
                    salesOrderDetail.RowguidProduct = reader.GetGuid(reader.GetOrdinal("Rowguid"));
                    salesOrderDetail.ModifiedDateProduct = reader.GetDateTime(reader.GetOrdinal("ModifiedDate"));

                    salesOrderDetailsWithProdcutInfo.Add(salesOrderDetail);
                }
                reader.Close();
            }
            return salesOrderDetailsWithProdcutInfo;
        }

        public List<GroupedOrderItemWithStatisticsViewModel> GetGroupedOrdersWithStatistics()
        {
            List<GroupedOrderItemWithStatisticsViewModel> groupedOrdersWithStatistics = new List<GroupedOrderItemWithStatisticsViewModel>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = @"SELECT SalesOrderID as OrderNumber,
                         COUNT(*) as NumberOfOrderItems,
                         SUM(LineTotal) as SumOfOrderItems,
                         AVG(LineTotal) as AverageOfOrderItems,
                         MAX(LineTotal) as MostExpensiveOrderItem,
                         MIN(LineTotal) as CheapestOrderItem,
                         MAX(LineTotal) - MIN(LineTotal) as RangeOfOrderItems
                       FROM [Sales].[SalesOrderDetail] s
                       GROUP BY SalesOrderID
                       ORDER BY NumberOfOrderItems DESC";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    GroupedOrderItemWithStatisticsViewModel groupedOrder = new GroupedOrderItemWithStatisticsViewModel();

                    groupedOrder.OrderNumber = reader.GetInt32(reader.GetOrdinal("OrderNumber"));
                    groupedOrder.NumberOfOrderItems = reader.GetInt32(reader.GetOrdinal("NumberOfOrderItems"));
                    groupedOrder.SumOfOrderItems = reader.GetDecimal(reader.GetOrdinal("SumOfOrderItems"));
                    groupedOrder.AverageOfOrderItems = reader.GetDecimal(reader.GetOrdinal("AverageOfOrderItems"));
                    groupedOrder.MostExpensiveOrderItem = reader.GetDecimal(reader.GetOrdinal("MostExpensiveOrderItem"));
                    groupedOrder.CheapestOrderItem = reader.GetDecimal(reader.GetOrdinal("CheapestOrderItem"));
                    groupedOrder.RangeOfOrderItems = reader.GetDecimal(reader.GetOrdinal("RangeOfOrderItems"));
                    //groupedOrder.DeviationFromAverage = reader.IsDBNull(reader.GetOrdinal("DeviationFromAverage")) ? -1 : reader.GetDouble(reader.GetOrdinal("DeviationFromAverage"));

                    groupedOrdersWithStatistics.Add(groupedOrder);
                }
                reader.Close();
            }
            return groupedOrdersWithStatistics;
        }

        public void GetTransactionProductsWithInformation()
        {
            //List<TransactionProductsWithInformationViewModel> transactionProducts = new List<TransactionProductsWithInformationViewModel>();
            using (_con = new SqlConnection(_connectionString))
            {
                string sqlQuery = @"SELECT TransactionID as TransactionID,
                         Product.ProductID as ProductID,
                         ProductPhoto.ThumbNailPhoto as ProductPhoto,
                         ProductDescription.Description as ProductDescription
                        FROM Production.TransactionHistory
                        JOIN Production.Product
	                        ON Production.TransactionHistory.ProductID = Production.Product.ProductID
                        JOIN Production.ProductProductPhoto
	                        ON Production.ProductProductPhoto.ProductID = Production.Product.ProductID
                        JOIN Production.ProductPhoto
	                        ON Production.ProductPhoto.ProductPhotoID = Production.ProductProductPhoto.ProductPhotoID
                        JOIN Production.ProductModel
	                        ON Production.Product.ProductModelID = Production.ProductModel.ProductModelID
                        JOIN Production.ProductModelProductDescriptionCulture
	                        ON Production.ProductModel.ProductModelID = Production.ProductModelProductDescriptionCulture.ProductModelID
                        JOIN Production.ProductDescription
	                        ON Production.ProductModelProductDescriptionCulture.ProductDescriptionID = Production.ProductDescription.ProductDescriptionID
                        WHERE ProductModelProductDescriptionCulture.CultureID = 'en'
                        ORDER BY TransactionID DESC";
                SqlCommand command = new SqlCommand(sqlQuery, (SqlConnection)_con);

                _con.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TransactionProductsWithInformationViewModel transactionProduct = new TransactionProductsWithInformationViewModel();

                    transactionProduct.TransactionID = !reader.IsDBNull(reader.GetOrdinal("TransactionID")) ? reader.GetInt32(reader.GetOrdinal("TransactionID")) : -1;
                    transactionProduct.ProductID = !reader.IsDBNull(reader.GetOrdinal("ProductID")) ? reader.GetInt32(reader.GetOrdinal("ProductID")) : -1;
                    transactionProduct.ProductPhoto = (byte[])reader["ProductPhoto"];
                    transactionProduct.ProductDescription = !reader.IsDBNull(reader.GetOrdinal("ProductDescription")) ? reader.GetString(reader.GetOrdinal("ProductDescription")) : string.Empty;

                    //transactionProducts.Add(transactionProduct);
                }
                reader.Close();
            }
            
        }
    }
}
