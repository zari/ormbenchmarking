﻿using ORMBenchmarking.Models.Model;
using ORMBenchmarking.Models.ViewModel;
using System.Collections.Generic;

namespace ORMBenchmarking.Dapper.Abstract
{
    public interface IDapperRepository
    {
        List<TransactionHistory> GetAllTransactions();
        List<TransactionHistoryArchive> GetAllArchiveTransactions();
        decimal GetSumOfAllTransactions();
        List<EmployeeAgeViewModel> GetEmployeesOlderThan50();
        List<SalesYearViewModel> GetSalesByYears();
        List<EmployeeQuantityPerDepartmentViewModel> GetEmployeeQuantityPerDepartment();
        List<SalesOrderWithProductInfoViewModel> GetSalesOrderDetailsWithProductInfo();
        List<GroupedOrderItemWithStatisticsViewModel> GetGroupedOrdersWithStatistics();
        void GetTransactionProductsWithInformation();
    }
}
