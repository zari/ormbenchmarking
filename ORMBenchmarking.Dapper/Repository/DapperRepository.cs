﻿using System.Linq;
using ORMBenchmarking.Dapper.Abstract;
using ORMBenchmarking.Models.Model;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using ORMBenchmarking.Models.ViewModel;
using System;

namespace ORMBenchmarking.Dapper.Repository
{
    public class DapperRepository : IDapperRepository
    {
        IDbConnection _con;

        public DapperRepository()
        {
            _con = new SqlConnection("Server=DESKTOP-H4PHU4K;Database=AdventureWorks2014;Trusted_Connection=True;");
        }

        public List<TransactionHistory> GetAllTransactions()
        {
            return _con.Query<TransactionHistory>("select * from Production.TransactionHistory").ToList();
            ///TODO: check the returned query, because if we put simple query it's only returning TransactionHistory object without 
            ///connected Product object. 
            //return _con.Query<TransactionHistory, Product, TransactionHistory>("select * from Production.TransactionHistory",
            //    (trHist, prod) => { trHist.Product = prod; return trHist; }).AsQueryable();
        }

        public List<TransactionHistoryArchive> GetAllArchiveTransactions()
        {
            return _con.Query<TransactionHistoryArchive>("select * from Production.TransactionHistoryArchive").ToList();
        }

        public decimal GetSumOfAllTransactions()
        {
            //return _con.Query<TransactionHistory>("select * from Production.TransactionHistory").Sum(x => x.Quantity * x.ActualCost);
            return _con.ExecuteScalar<decimal>("select SUM(Quantity * ActualCost) from Production.TransactionHistory");
        }

        public List<EmployeeAgeViewModel> GetEmployeesOlderThan50()
        {
            var cutOffDate = DateTime.Today.AddYears(-50);
            string sqlQuery = @"select Person.BusinessEntityID as PersonID, Person.LastName,
                        Person.FirstName,
                        Employee.BusinessEntityID as EmployeeID,
                        Employee.Gender,
                        Employee.BirthDate,
                        Employee.HireDate,
                        DATEDIFF(YEAR, Employee.BirthDate, GETDATE()) as Age
                    FROM Person.Person
                        JOIN HumanResources.Employee
                        ON Person.BusinessEntityID = Employee.BusinessEntityID
                    WHERE Employee.BirthDate <= @cutOffDate
                    ORDER BY Age DESC";

            var employeesAge = _con.Query<Person, Employee, EmployeeAgeViewModel>(sqlQuery,
                (per, emp) => new EmployeeAgeViewModel()
                {
                    FirstName = per.FirstName,
                    LastName = per.LastName,
                    Gender = emp.Gender,
                    BirthDate = emp.BirthDate,
                    HireDate = emp.HireDate,
                    Age = emp.Age
                },
                param: new { cutOffDate },
                splitOn: "EmployeeID")
                .ToList();

            return employeesAge;
        }

        public List<SalesYearViewModel> GetSalesByYears()
        {
            string sqlQuery = @"select YEAR(OrderDate) AS YearOfSummary,
                        COUNT(*) AS OrdersQuantity,
                        SUM(SubTotal) AS OrdersPriceSum
                    FROM Sales.SalesOrderHeader  
                    GROUP BY YEAR(OrderDate)
                    ORDER BY YearOfSummary";

            return _con.Query<SalesYearViewModel>(sqlQuery).ToList();
        }

        public List<EmployeeQuantityPerDepartmentViewModel> GetEmployeeQuantityPerDepartment()
        {
            string sqlQuery = @"SELECT HumanResources.Department.Name as DepartmentName,
	                    COUNT(*) as EmployeeQuantity
                    FROM HumanResources.EmployeeDepartmentHistory 
	                    INNER JOIN HumanResources.Department
	                    ON HumanResources.EmployeeDepartmentHistory.DepartmentID = HumanResources.Department.DepartmentID
                    WHERE HumanResources.EmployeeDepartmentHistory.EndDate IS NULL
                    GROUP BY HumanResources.Department.Name
                    ORDER BY EmployeeQuantity DESC";

            return _con.Query<EmployeeQuantityPerDepartmentViewModel>(sqlQuery).ToList();
        }

        public List<SalesOrderWithProductInfoViewModel> GetSalesOrderDetailsWithProductInfo()
        {
            string sqlQuery = @"SELECT * FROM Sales.SalesOrderDetail s
                        INNER JOIN Production.Product p ON s.ProductID = p.ProductID
                        ORDER BY s.ProductID DESC, s.SalesOrderID";

            var salesOrderWithProductInfo = _con.Query<SalesOrderDetail, Product, SalesOrderWithProductInfoViewModel>(sqlQuery,
               (orderDetail, product) => new SalesOrderWithProductInfoViewModel()
               {
                   SalesOrderId = orderDetail.SalesOrderId,
                   SalesOrderDetailId = orderDetail.SalesOrderDetailId,
                   CarrierTrackingNumber = orderDetail.CarrierTrackingNumber,
                   OrderQty = orderDetail.OrderQty,
                   ProductId = orderDetail.ProductId,
                   SpecialOfferId = orderDetail.SpecialOfferId,
                   UnitPrice = orderDetail.UnitPrice,
                   UnitPriceDiscount = orderDetail.UnitPriceDiscount,
                   LineTotal = orderDetail.LineTotal,
                   RowguidSalesOrder = orderDetail.Rowguid,
                   ModifiedDateSalesOrder = orderDetail.ModifiedDate,
                   Name = product.Name,
                   ProductNumber = product.ProductNumber,
                   MakeFlag = product.MakeFlag,
                   FinishedGoodsFlag = product.FinishedGoodsFlag,
                   Color = product.Color,
                   SafetyStockLevel = product.SafetyStockLevel,
                   ReorderPoint = product.ReorderPoint,
                   StandardCost = product.StandardCost,
                   ListPrice = product.ListPrice,
                   Size = product.Size,
                   SizeUnitMeasureCode = product.SizeUnitMeasureCode,
                   WeightUnitMeasureCode = product.WeightUnitMeasureCode,
                   Weight = product.Weight,
                   DaysToManufacture = product.DaysToManufacture,
                   ProductLine = product.ProductLine,
                   Class = product.Class,
                   Style = product.Style,
                   ProductSubcategoryId = product.ProductSubcategoryId,
                   ProductModelId = product.ProductModelId,
                   SellStartDate = product.SellStartDate,
                   SellEndDate = product.SellEndDate,
                   DiscontinuedDate = product.DiscontinuedDate,
                   RowguidProduct = product.Rowguid,
                   ModifiedDateProduct = product.ModifiedDate
               },
               splitOn: "ProductID")
               .ToList();

            return salesOrderWithProductInfo;
        }

        public List<GroupedOrderItemWithStatisticsViewModel> GetGroupedOrdersWithStatistics()
        {
            string sqlQuery = @"SELECT SalesOrderID as OrderNumber,
                         COUNT(*) as NumberOfOrderItems,
                         SUM(LineTotal) as SumOfOrderItems,
                         AVG(LineTotal) as AverageOfOrderItems,
                         MAX(LineTotal) as MostExpensiveOrderItem,
                         MIN(LineTotal) as CheapestOrderItem,
                         MAX(LineTotal) - MIN(LineTotal) as RangeOfOrderItems
                       FROM [Sales].[SalesOrderDetail] s
                       GROUP BY SalesOrderID
                       ORDER BY NumberOfOrderItems DESC";

            return _con.Query<GroupedOrderItemWithStatisticsViewModel>(sqlQuery).ToList();
        }

        public void GetTransactionProductsWithInformation()
        {
            string sqlQuery = @"SELECT TransactionID as TransactionID,
                         Product.ProductID as ProductID,
                         ProductPhoto.ThumbNailPhoto as ProductPhoto,
                         ProductDescription.Description as ProductDescription
                        FROM Production.TransactionHistory
                        JOIN Production.Product
	                        ON Production.TransactionHistory.ProductID = Production.Product.ProductID
                        JOIN Production.ProductProductPhoto
	                        ON Production.ProductProductPhoto.ProductID = Production.Product.ProductID
                        JOIN Production.ProductPhoto
	                        ON Production.ProductPhoto.ProductPhotoID = Production.ProductProductPhoto.ProductPhotoID
                        JOIN Production.ProductModel
	                        ON Production.Product.ProductModelID = Production.ProductModel.ProductModelID
                        JOIN Production.ProductModelProductDescriptionCulture
	                        ON Production.ProductModel.ProductModelID = Production.ProductModelProductDescriptionCulture.ProductModelID
                        JOIN Production.ProductDescription
	                        ON Production.ProductModelProductDescriptionCulture.ProductDescriptionID = Production.ProductDescription.ProductDescriptionID
                        WHERE ProductModelProductDescriptionCulture.CultureID = 'en'
                        ORDER BY TransactionID DESC";

           _con.Query<TransactionProductsWithInformationViewModel>(sqlQuery);
        }
    }
}
