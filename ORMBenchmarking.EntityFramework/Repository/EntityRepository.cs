﻿using Microsoft.EntityFrameworkCore;
using ORMBenchmarking.EntityFramework.Abstract;
using ORMBenchmarking.Models.Model;
using ORMBenchmarking.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ORMBenchmarking.EntityFramework.Repository
{
    public class EntityRepository : IEntityRepository
    {
        //TODO: change to interface instead of class in future
        private AdventureWorks2014Context _context;

        public EntityRepository()
        {
            _context = new AdventureWorks2014Context();
        }

        public List<TransactionHistory> GetAllTransactions()
        {
            return _context.TransactionHistory.Select(x => x).ToList();
        }

        public List<TransactionHistoryArchive> GetAllArchiveTransactions()
        {
            return _context.TransactionHistoryArchive.Select(x => x).ToList();
        }

        public decimal GetSumOfAllTransactions()
        {
            return _context.TransactionHistory.Sum(x => x.Quantity * x.ActualCost);
        }

        public List<EmployeeAgeViewModel> GetEmployeesOlderThan50()
        {
            return _context.Employee
                 .Join(_context.Person,
                     emp => emp.BusinessEntityId,
                     per => per.BusinessEntityId,
                     (emp, per) => new EmployeeAgeViewModel()
                     {
                         FirstName = per.FirstName,
                         LastName = per.LastName,
                         Gender = emp.Gender,
                         BirthDate = emp.BirthDate,
                         HireDate = emp.HireDate,
                         Age = EF.Functions.DateDiffYear(emp.BirthDate, DateTime.Now)
                     })
                     .Where(x => EF.Functions.DateDiffDay(x.BirthDate, DateTime.Now) >= 18262) // 18262 is 50 years in days.
                     .OrderByDescending(x => x.Age)
                     .ToList();
        }

        public List<SalesYearViewModel> GetSalesByYears()
        {
            return _context.SalesOrderHeader
                .GroupBy(g => g.OrderDate.Year)
                .Select(x => new SalesYearViewModel()
                {
                    YearOfSummary = x.Key,
                    OrdersQuantity = x.Count(),
                    OrdersPriceSum = x.Sum(c => c.SubTotal)
                })
                .OrderBy(x => x.YearOfSummary)
                .ToList();
        }

        public List<EmployeeQuantityPerDepartmentViewModel> GetEmployeeQuantityPerDepartment()
        {
            return _context.Department
                .GroupJoin(
                    _context.EmployeeDepartmentHistory.Where(x => !x.EndDate.HasValue),
                    dep => dep.DepartmentId,
                    depHist => depHist.DepartmentId,
                    (dep, depHist) => new EmployeeQuantityPerDepartmentViewModel()
                    {
                        DepartmentName = dep.Name,
                        EmployeeQuantity = depHist.Count()
                    })
                    .OrderByDescending(x => x.EmployeeQuantity)
                    .ToList();
        }

        public List<SalesOrderWithProductInfoViewModel> GetSalesOrderDetailsWithProductInfo()
        {
            _context.Database.SetCommandTimeout(300);
            return _context.SalesOrderDetail
                 .Join(_context.Product,
                     orderDetail => orderDetail.ProductId,
                     product => product.ProductId,
                     (orderDetail, product) => new SalesOrderWithProductInfoViewModel()
                     {
                         SalesOrderId = orderDetail.SalesOrderId,
                         SalesOrderDetailId = orderDetail.SalesOrderDetailId,
                         CarrierTrackingNumber = orderDetail.CarrierTrackingNumber,
                         OrderQty = orderDetail.OrderQty,
                         ProductId = orderDetail.ProductId,
                         SpecialOfferId = orderDetail.SpecialOfferId,
                         UnitPrice = orderDetail.UnitPrice,
                         UnitPriceDiscount = orderDetail.UnitPriceDiscount,
                         LineTotal = orderDetail.LineTotal,
                         RowguidSalesOrder = orderDetail.Rowguid,
                         ModifiedDateSalesOrder = orderDetail.ModifiedDate,
                         Name = product.Name,
                         ProductNumber = product.ProductNumber,
                         MakeFlag = product.MakeFlag,
                         FinishedGoodsFlag = product.FinishedGoodsFlag,
                         Color = product.Color,
                         SafetyStockLevel = product.SafetyStockLevel,
                         ReorderPoint = product.ReorderPoint,
                         StandardCost = product.StandardCost,
                         ListPrice = product.ListPrice,
                         Size = product.Size,
                         SizeUnitMeasureCode = product.SizeUnitMeasureCode,
                         WeightUnitMeasureCode = product.WeightUnitMeasureCode,
                         Weight = product.Weight,
                         DaysToManufacture = product.DaysToManufacture,
                         ProductLine = product.ProductLine,
                         Class = product.Class,
                         Style = product.Style,
                         ProductSubcategoryId = product.ProductSubcategoryId,
                         ProductModelId = product.ProductModelId,
                         SellStartDate = product.SellStartDate,
                         SellEndDate = product.SellEndDate,
                         DiscontinuedDate = product.DiscontinuedDate,
                         RowguidProduct = product.Rowguid,
                         ModifiedDateProduct = product.ModifiedDate
                     })
                     .OrderByDescending(x => x.ProductId)
                     .ThenBy(x => x.SalesOrderId)
                     .ToList();
        }

        public List<GroupedOrderItemWithStatisticsViewModel> GetGroupedOrdersWithStatistics()
        {
            //var stDev = _context.StDEV.FromSql(@"SELECT SalesOrderID as OrderNumber, COUNT(*) as NumberOfOrderItems, STDEV(LineTotal) AS Value FROM [Sales].[SalesOrderDetail] GROUP BY 
            //SalesOrderID ORDER BY NumberOfOrderItems DESC");
            return _context.SalesOrderDetail
                .GroupBy(x => x.SalesOrderId)
                .Select(x => new GroupedOrderItemWithStatisticsViewModel()
                {
                    OrderNumber = x.Key,
                    NumberOfOrderItems = x.Count(),
                    SumOfOrderItems = x.Sum(c => c.LineTotal),
                    AverageOfOrderItems = x.Average(c => c.LineTotal),
                    MostExpensiveOrderItem = x.Max(c => c.LineTotal),
                    CheapestOrderItem = x.Min(c => c.LineTotal),
                    RangeOfOrderItems = x.Max(c => c.LineTotal) - x.Min(c => c.LineTotal),
                    //DeviationFromAverage = GetStandardDeviation(x.Average(c => c.LineTotal), x) // -1 //how to do standard deviation here
                })
                .OrderByDescending(x => x.NumberOfOrderItems)
                .ToList();
        }
        public double GetStandardDeviation(decimal avg, IGrouping<int, SalesOrderDetail> grouping)
        {
            decimal sumOfSquaresDifferences = grouping.Select(val => (val.LineTotal - avg) * (val.LineTotal - avg)).Sum();
            if (sumOfSquaresDifferences > 0)
            {
                var stDev = Math.Sqrt(Convert.ToDouble(sumOfSquaresDifferences / (grouping.Count() - 1)));
                return stDev;
            }
            else
            {
                return 0;
            }
        }

        public void GetTransactionProductsWithInformation()
        {
            _context.Database.SetCommandTimeout(300);
            _context.Product
               .Join(_context.TransactionHistory,
                   prod => prod.ProductId,
                   transHist => transHist.ProductId,
                   (transHist, prod) => new { transHist, prod })
               .Join(_context.ProductProductPhoto,
                   o => o.prod.ProductId,
                   prodProdPhoto => prodProdPhoto.ProductId,
                   (prod1, prodProdPhoto) => new { prod1, prodProdPhoto })
               .Join(_context.ProductPhoto,
                   o => o.prodProdPhoto.ProductPhotoId,
                   prodPhoto => prodPhoto.ProductPhotoId,
                   (prod2, prodPhoto) => new { prod2, prodPhoto })
               .Join(_context.ProductModel,
                   o => o.prod2.prod1.prod.Product.ProductModelId,
                   prodModel => prodModel.ProductModelId,
                   (prod3, prodModel) => new { prod3, prodModel })
               .Join(_context.ProductModelProductDescriptionCulture,
                   o => o.prodModel.ProductModelId,
                   prodModelDesc => prodModelDesc.ProductModelId,
                   (prod4, prodModelDesc) => new { prod4, prodModelDesc })
               .Join(_context.ProductDescription,
                   o => o.prodModelDesc.ProductDescriptionId,
                   prodDesc => prodDesc.ProductDescriptionId,
                   (prod5, prodDesc) => new { prod5, prodDesc })
               .Where(x => x.prod5.prodModelDesc.CultureId == "en")
               .Select(x => new TransactionProductsWithInformationViewModel()
               {
                   ProductID = x.prod5.prod4.prod3.prod2.prod1.prod.ProductId,
                   TransactionID = x.prod5.prod4.prod3.prod2.prod1.prod.TransactionId,
                   ProductDescription = x.prodDesc.Description,
                   ProductPhoto = x.prod5.prod4.prod3.prodPhoto.ThumbNailPhoto
               })
               .OrderByDescending(x => x.TransactionID)
               .ToList();
        }
    }
}
