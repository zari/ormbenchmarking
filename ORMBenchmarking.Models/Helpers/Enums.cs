﻿using System.ComponentModel.DataAnnotations;

namespace ORMBenchmarking.Models.Helpers
{
    public static class Enums
    {
        public enum QueryType
        {
            [Display(Name="Lista transakcji")]
            Query_1 = 0,
            [Display(Name="Lista transakcji z historii")]
            Query_2 = 1,
            [Display(Name="Całkowity koszt produktów ze wszystkich transakcji")]
            Query_3 = 2,
            [Display(Name="Lista pracowników mających więcej niż 50 lat")]
            Query_4 = 3,
            [Display(Name="Całkowita liczba transakcji sprzedaży wraz z wartością sprzedanych produktów")]
            Query_5 = 4,
            [Display(Name="Liczba pracowników w poszczególnych działach")]
            Query_6 = 5,
            [Display(Name ="Informacje o zamówieniach wraz z informacjami o powiązanych produktach")]
            Query_7 = 6,
            [Display(Name ="Zamówienia zgrupowane względem numerów wraz z liczbą pozycji oraz innymi statystykami")]
            Query_8 = 7,
            [Display(Name = "Transakcje wraz z powiązanym produktem, jego zdjęciem oraz opisem (wielokrotny JOIN)")]
            Query_9 = 8,
            //Query_10 = 9
        }

        public enum LibraryType
        {
            EntityFramework = 0,
            NHibernate = 1,
            Dapper = 2,
            Adonet = 3
        }

        public enum Gender
        {
            M = 0,
            F = 1
        }
    }
}
