﻿namespace ORMBenchmarking.Models.ViewModel
{
    public class GroupedOrderItemWithStatisticsViewModel
    {
        public int OrderNumber { get; set; }
        public int NumberOfOrderItems { get; set; }
        public decimal SumOfOrderItems { get; set; }
        public decimal AverageOfOrderItems { get; set; }
        public double AverageOfOrderItemsDouble { get; set; }
        public decimal MostExpensiveOrderItem { get; set; }
        public decimal CheapestOrderItem { get; set; }
        public decimal RangeOfOrderItems { get; set; }
        public double DeviationFromAverage { get; set; }
    }
}
