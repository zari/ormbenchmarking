﻿using System.Collections.Generic;
using static ORMBenchmarking.Models.Helpers.Enums;

namespace ORMBenchmarking.Models.ViewModel
{
    public class ExecutionTimeViewModel
    {
        public ExecutionTimeViewModel(Dictionary<LibraryType, List<double>> queryExecutionTime, QueryType queryType)
        {
            QueryExecutionTime = queryExecutionTime;
            QueryType = queryType;
        }
        public Dictionary<LibraryType, List<double>> QueryExecutionTime { get; set; }
        public QueryType QueryType;
    }
}
