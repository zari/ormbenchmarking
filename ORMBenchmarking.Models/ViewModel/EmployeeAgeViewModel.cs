﻿using System;
using static ORMBenchmarking.Models.Helpers.Enums;

namespace ORMBenchmarking.Models.ViewModel
{
    public class EmployeeAgeViewModel
    {
        public EmployeeAgeViewModel()
        {
                
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HireDate { get; set; }
        public int Age { get; set; }
    }
}
