﻿namespace ORMBenchmarking.Models.ViewModel
{
    public class EmployeeQuantityPerDepartmentViewModel
    {
        public EmployeeQuantityPerDepartmentViewModel()
        {

        }

        public string DepartmentName { get; set; }
        public int EmployeeQuantity { get; set; }
    }
}
