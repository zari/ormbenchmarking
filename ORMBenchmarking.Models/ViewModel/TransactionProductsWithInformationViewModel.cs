﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ORMBenchmarking.Models.ViewModel
{
    public class TransactionProductsWithInformationViewModel
    {
        public int TransactionID { get; set; }
        public int ProductID { get; set; }
        public byte[] ProductPhoto { get; set; }
        public string ProductDescription { get; set; }
    }
}
