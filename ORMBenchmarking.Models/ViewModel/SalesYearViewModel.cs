﻿namespace ORMBenchmarking.Models.ViewModel
{
    public class SalesYearViewModel
    {
        public SalesYearViewModel()
        {

        }

        public int YearOfSummary { get; set; }
        public int OrdersQuantity { get; set; }
        public decimal OrdersPriceSum { get; set; }
    }
}
